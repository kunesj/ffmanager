
APTGET_RUN_DEP=python3 python3-pip python3-pyqt5 python3-lxml
PIP_RUN_DEP=requests beautifulsoup4 natsort colorlog

APTGET_TEST_DEP=python3-nose python3-coverage
PIP_TEST_DEP=

ARCH=$(shell arch)
VERSION=$(shell python3 -c "import ffmanager; print(ffmanager.__version__)")

help:
	@echo "clean -> Will remove some files crated by install"
	@echo "install -> Will install application on system"
	@echo "uninstall -> Will uninstall application from system (does not uninstall dependencies)"
	@echo "install_dep -> Will install application dependencies with apt-get and pip"
	@echo "install_test_dep -> Will install dependencies for testing with apt-get and pip"
	@echo "test -> Will run tests"

all: clean install_dep install install_test_dep

clean:
	sudo rm -rf build dist

install: clean
	sudo python3 setup.py build install
	sudo cp ffreader.desktop /usr/share/applications/ffreader.desktop
	sudo cp ffstatus.desktop /usr/share/applications/ffstatus.desktop

uninstall:
	sudo pip3 uninstall -y ffmanager
	sudo rm -f /usr/share/applications/ffreader.desktop
	sudo rm -f /usr/share/applications/ffstatus.desktop

reinstall: uninstall install

install_dep:
	sudo apt-get install $(APTGET_RUN_DEP)
	sudo pip3 install $(PIP_RUN_DEP)

install_test_dep:
	sudo apt-get install $(APTGET_TEST_DEP)
	#sudo pip3 install $(PIP_TEST_DEP)

test:
	# Dynamicaly adds development folder to python path, for correct sub-package imports in development.
	nosetests3 -v --nocapture --with-coverage --cover-package=ffmanager,ffreader,ffstatus

## For Development Only
#######################

run_ffmanager:
	python3 -m ffmanager

run_ffreader:
	python3 -m ffmanager.ffreader

