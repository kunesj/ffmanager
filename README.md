FFManager (FanFiction Manager)
==============================

Small script for keeping fiction and fanfiction downloaded from internet updated.

```
usage: __main__.py [-h] [-u URL] [-c] [-d] [-r N [N ...]] [-f] [--debug]
                   [--logfile]
                   [path]

FFManager

Small script for keeping fiction and fanfiction downloaded from internet updated.

positional arguments:
  path                  Path to folder with works or path to work

optional arguments:
  -h, --help            show this help message and exit
  -u URL, --url URL     Url of new fanfiction series to download (requires --download for actual download of chapters)
  -c, --check           Will check for updates (downloads metadata)
  -d, --download        Will download new chapters (forces --check)
  -r N [N ...], --range N [N ...]
                        Range of chapters to download [1, -1], range starts at 1.
  -f, --force           Will disable skipping of recently updated works
  --debug               Debug mode
  --logfile             Save log to file
```

Includes simple HTML reader application.

```
usage: ffreader [-h] [path]

FFreader

Read downloaded (very simple) HTML files. Must be encoded in UTF-8 (or just ASCII).

Shortcuts:
  i - zoom in
  o - zoom out
  p - zoom reset
  n - change skin (night mode)
  f - maximize window
  s - enable search
  q - load last file in directory
  w - load next file in directory
  b - browse dir

positional arguments:
  path        Path to HTML file or directory with files to read

optional arguments:
  -h, --help  show this help message and exit
```

Small script that prints list of fanfiction that have unread chapters.

```
usage: ffstatus [-h] [-p PATH] [--gui] [--debug] [--logfile]

FFStatus

List reading status of unfinished series.

optional arguments:
  -h, --help            show this help message and exit
  -p PATH, --path PATH  Will check for fanfiction in given PATH and then recursively search child folders for any other fanfiction series.
  --gui                 Gui mode
  --debug               Debug mode
  --logfile             Save log to file
```


Linux Install (Debian/Ubuntu/Mint)
----------------------------------

For quick install of application and it's dependencies with:
```
make all
```
(This works only on Debian based systems)

For more information run:
```
make help
```

Windows/Mac Install
-------------------

Manual only

1. Install Python3

2. Install dependencies, you can find them on first few lines in Makefile file.

3. You should now be able to start this by running any main scripts in ffmanager and ffreader folders. (from commandline)

Example:
```
python3 -m C:\\Source\ffmanager\ffreader --help
```
