#!/usr/bin/env python3
# encoding: utf-8

from setuptools import setup, find_packages
import os

import ffmanager

setup(
    name='ffmanager',
    version=ffmanager.__version__,
    description='FanFiction Manager',
    long_description=open(os.path.join(os.path.dirname(__file__), 'README.md')).read(),
    author='Jiří Kuneš',
    author_email='jirka642@gmail.com',
    url='https://gitlab.com/kunesj/ffmanager',
    download_url='https://gitlab.com/kunesj/ffmanager/-/archive/master/ffmanager-master.zip',
    keywords=['fanfiction', 'download', 'manager', 'reader'],
    packages=find_packages(),
    include_package_data=True,
    license='GPL3',
    entry_points={
        'console_scripts': [
            'ffmanager = ffmanager.__main__:main',
            'ffstatus = ffmanager.ffstatus.__main__:main',
            'ffreader = ffmanager.ffreader.__main__:main'
        ],
    },
    install_requires=[
      'setuptools',
      'beautifulsoup4',
      'requests',
      'qdarkstyle',
      'colorlog',
      'cloudscraper',
      # apt-get install python3-pyqt5 python3-lxml
    ],
)
