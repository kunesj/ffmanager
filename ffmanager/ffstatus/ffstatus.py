#!/usr/bin/env python3
# encoding: utf-8

import logging
import os
import natsort
from bs4 import BeautifulSoup

from ..database import Database

_logger = logging.getLogger(__name__)


class FFStatus(object):
    """
    ffstatus.ffm
    ============

    Commented lines:
        Start with '#' or '-'

    Commented blocks:
        Are between lines that start with '/**' and end with '**/'

    Example of line:
        [W,H]Anime_MangaNaruto_-_Team_8 - ch1-ch24 - end ---> good piece of work
        [SERIES_STATUS]SERIES_NAME - READ_CHAPTERS - CHAPTER_PROGRESS ---> NOTE

        There should only be ' - ' twice in one line before CHAPTER_PROGRESS part.
        ' - ' can be padded with spaces on sides to allow indenting.
        ' ---> ' and text behind it is optional.

    SERIES_STATUS:
        There can be any string before [STATUS] with exception of ones starting
        with '#', '-' or '/**'

        [ALL] == All read (can also be [すべて] or [SUBETE])
        [S] == Reading / Stalled (can also be [R])
        [W] == Waiting for new chapters
        [D] == Dropped
        [H] == Author Hiatus
        [_] == Want to read

    SERIES_NAME:
        Has all spaces replaced with '_'

    READ_CHAPTERS:
        Is in format 'ch1-chX' where X is last read chapter.
        Can have some notes added at the end between '[]', for example: 'ch1-ch40[beta]'

    CHAPTER_PROGRESS:
        Will be 'end' unless stopped reading in the middle of chapter

    """

    def __init__(self, ffpath=None, ffstatus_path=None):
        # path to root folder with fictions
        self.ffpath = ffpath
        self.database = Database(ffpath)
        # path to ffstatus file
        self.ffstatus_path = ffstatus_path

        # lines loaded and parsed from ffstatus file
        self.lines = []
        self.lines_parsed = []

        # fiction info lines and type
        self.lines_info = []
        self.lines_type = []

        # line visibility: for filtering
        self.line_visibility = []

        # html
        self.html_css_style = '''
            body{color:black;background:#161c1f;}
            table{margin:-4px;pedding:4px;border-spacing:0;}
            td, th {padding:0;padding-left:2px;padding-right:2px;}
            .empty{background:#2a363b;}
            .read{background:#546c76;}
            .unread{background:#99b898;}
            .unknown{background:#fecea8;}
            .error{background:#e84a5f;}
            '''

    @staticmethod
    def parse_ffstatus_line(ffstatus_line):
        l = ffstatus_line.strip()

        # get parts of series status
        l_spl = l.split(' - ')
        status_and_name = l_spl[0].strip()
        read = l_spl[1].strip()
        progress = (' - '.join(l_spl[2:])).strip()
        note = ''
        if len(progress.split(' ---> ')) >= 2:
            note = progress.split(' ---> ')[1].strip()
            progress = progress.split(' ---> ')[0].strip()

        status = status_and_name.split(']')[0].split('[')[1]
        name = ']'.join(status_and_name.split(']')[1:])

        # parse read part
        last_read = None
        last_read_title = None
        try:
            # 'ch1-chX' --> ['','1-','X']
            if read.startswith('ch'):
                last_read = read.split('[')[0].strip().split('ch')[2].strip()
                last_read = int(last_read) if last_read.isdigit() else None

                if '["' in read and '"]' in read:
                    last_read_title = read.split('["')[1].split('"]')[0]
            else:
                raise Exception('Unknown READ_CHAPTERS format')

        except Exception:
            print('Unable to parse read chapter from "{}" on line: {}'.format(read, l))

        # Parse status part
        status_par = [x.strip() for x in status.split(',')]
        for i in range(len(status_par)):
            if status_par[i] == 'SUBETE' or status_par[i] == 'すべて':
                status_par[i] = 'ALL'
            elif status_par[i] == 'R':
                status_par[i] = 'S'

        # return processed line
        return {
            'status': status_par, 'name': name, 'last_read': last_read, 'last_read_title': last_read_title,
            'progress': progress, 'note': note
            }

    @staticmethod
    def parsed_ffstatus_line_to_string(parsed_ffstatus_line):
        pl = parsed_ffstatus_line
        return '[{}]{} - ch1-ch{} - {}'.format(','.join(pl['status']), pl['name'], pl['last_read'], pl['progress'])

    @classmethod
    def read_ffstatus_file(cls, ffstatus_path):
        ffstatus_path = os.path.abspath(ffstatus_path)
        if not os.path.isfile(ffstatus_path):
            raise Exception('Bad ffstatus.ffm file path: '+ffstatus_path)

        with open(ffstatus_path, 'r') as f:
            lines = f.readlines()

        parsed_lines = []
        comment_block = False
        for l_idx, l in enumerate(lines):
            try:
                l = l.strip()

                # skip commented blocks
                if l.startswith('/**'):
                    comment_block = True
                if l.endswith('**/'):
                    comment_block = False
                    continue
                if comment_block is True:
                    continue

                # skip commented or empty lines
                if l.startswith('#') or l.startswith('-') or l == '':
                    continue

                # parse line
                pl = cls.parse_ffstatus_line(l)
                pl['line_index'] = l_idx
                parsed_lines.append(pl)

            except Exception:
                _logger.exception('Unable to parse line: "{}"'.format(l))

        return parsed_lines

    def load_ffstatus_file(self, ffstatus_path):
        ffstatus_path = os.path.abspath(ffstatus_path)
        if not os.path.isfile(ffstatus_path):
            raise Exception('Bad ffstatus.ffm file path: '+ffstatus_path)

        with open(ffstatus_path, 'r') as f:
            self.lines = [ l.strip() for l in f.readlines() ]
        self.lines_parsed = [None,]*len(self.lines)
        for parsed_line in self.read_ffstatus_file(ffstatus_path):
            self.lines_parsed[parsed_line['line_index']] = parsed_line

        # reinit other variables
        self.lines_info = ['', ]*len(self.lines)
        self.lines_type = ['', ]*len(self.lines)
        self.line_visibility = [True, ]*len(self.lines)

    def compute_read_status(self):
        self.lines_info = []
        self.lines_type = []
        for idx, pl in enumerate(self.lines_parsed):
            if pl is None:
                info_line = ''
                info_format = 'empty'

            elif pl['name'].strip() not in self.database.works:  # name: ffinfo not found
                info_format = 'unknown'
                info_line = 'fiction not found'

            else:
                work = self.database.works[pl['name'].strip()]
                info_line = '{} ; ch{}'.format(work.update_datetime.date() if work.update_datetime else None, work.chapters)

                if pl['last_read'] is None and pl['last_read_title']:
                    last_read_title_norm = pl['last_read_title'].lower().replace(' ', '').replace('\t', '').replace('\n', '')

                    for filename in natsort.natsorted(os.listdir(work.path)):
                        if not any(filename.endswith(x) for x in ['.html', '.htm']):
                            continue
                        filepath = os.path.join(work.path, filename)

                        try:
                            with open(filepath, 'r') as f:
                                html = f.read()
                            soup = BeautifulSoup(html, 'lxml')
                            text_norm = soup.text.lower().replace(' ', '').replace('\t', '').replace('\n', '')

                            if last_read_title_norm in text_norm[:50+len(last_read_title_norm)*2]:
                                last_read = filename.split('.')[0].split('_')[0]
                                if last_read.isdigit():
                                    pl['last_read'] = int(last_read)
                                    break
                        except Exception:
                            _logger.exception('Failed to parse file "{}"'.format(filepath))
                            continue

                if pl['last_read'] is None:  # last read: parsing error
                    info_format = 'error'
                elif pl['last_read'] >= work.chapters:
                    info_format = 'read'
                else:
                    info_format = 'unread'

            self.lines_info.append(info_line)
            self.lines_type.append(info_format)

    def apply_filter(self, filter_text=''):
        filters_positive = []
        filters_negative = []
        for filter_str in filter_text.strip().split(' '):
            # detect negative filters
            negative = True if filter_str.startswith('-') else False
            if negative:
                filter_str = filter_str[1:]

            if negative:
                filters_negative.append(filter_str)
            else:
                filters_positive.append(filter_str)

        # detect which lines should be filtered out
        visible = [True]*len(self.lines)

        for idx in range(len(self.lines)):
            for f in filters_positive:
                visible[idx] = (visible[idx]) and (f.lower() in self.lines[idx].lower())

        for idx in range(len(self.lines)):
            for f in filters_negative:
                visible[idx] = (visible[idx]) and (f.lower() not in self.lines[idx].lower())

        self.line_visibility = visible

    def reload(self, filter_text=''):
        if self.ffpath is None or self.ffstatus_path is None:
            _logger.error('ffpath or ffstatus_path is None!')
            return

        self.load_ffstatus_file(self.ffstatus_path)
        self.database.load_works()
        self.compute_read_status()
        self.apply_filter(filter_text=filter_text)

    def to_html(self):
        lines_status = []
        lines_info = []
        for idx in range(len(self.lines_info)):
            if not self.line_visibility[idx]:
                continue
            lines_status.append('<tr class="{}"><td>{}</td></tr>'.format(self.lines_type[idx], self.lines[idx]))
            lines_info.append('<tr class="{}"><td>{}</td></tr>'.format(self.lines_type[idx], self.lines_info[idx]))

        status = '\n'.join(lines_status)
        info = '\n'.join(lines_info)

        status_html = '''<html><head>\
            <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
            <style>{}</style>
            </head><body><table cellspacing="0">{}</table></body></html>'''.format(self.html_css_style, status)
        info_html = '''<html><head>
            <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
            <style>{}</style>
            </head><body><table cellspacing="0">{}</table></body></html>'''.format(self.html_css_style, info)

        return status_html, info_html

    def to_plain_text(self):
        # lines variable
        l = []

        # loading ffstatus,ffinfo -> DONE
        l.append('---FFINFO-MISSING--------------------------------------------------')
        # finding ffinfo.ffm for every fanfiction
        for idx in range(len(self.lines)):
            if self.lines_type[idx] == 'unknown':
                l.append('Couldn\'t find ffinfo.ffm for fanfiction name: {}'.format(self.lines_parsed[idx]['name']))
        l.append('---NOT-IN-FFSTATUS-------------------------------------------------')
        # Couldn't find status line for fanfiction %s
        for ffname in self.database.works:
            found = False
            for lp in self.lines_parsed:
                if lp is None:
                    continue
                if lp['name'] == ffname:
                    found = True
                    break
            if not found:
                l.append('Couldn\'t find status line for fanfiction: {}'.format(ffname))
        l.append('---ERRORS----------------------------------------------------------')
        # Print errors
        data = []
        offset = 0
        for idx in range(len(self.lines)):
            if self.lines_type[idx] == 'error':
                offset = max(offset, len(self.lines_info[idx]))
                data.append([self.lines_info[idx], self.lines[idx].split(' ---> ')[0].strip()])
        for d in data:
            tab = ' '*(offset-len(d[0]))
            l.append('{}{} | {}'.format(d[0], tab, d[1]))
        l.append('---UNREAD----------------------------------------------------------')
        # Print series with different number of chapters read and existing
        data = []
        offset = 0
        for idx in range(len(self.lines)):
            if self.lines_type[idx] == 'unread':
                offset = max(offset, len(self.lines_info[idx]))
                data.append([self.lines_info[idx], self.lines[idx].split(' ---> ')[0].strip()])
        for d in data:
            tab = ' '*(offset-len(d[0]))
            l.append('{}{} | {}'.format(d[0], tab, d[1]))
        l.append('-------------------------------------------------------------------')

        return l
