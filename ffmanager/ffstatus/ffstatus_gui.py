#!/usr/bin/env python3
# encoding: utf-8

import logging
logger = logging.getLogger(__name__)

from PyQt5 import QtCore
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QMainWindow, QVBoxLayout, QTextEdit, QPushButton, \
    QWidget, QLabel, QHBoxLayout, QLineEdit


class FFStatusWindow(QMainWindow):

    def __init__(self, ffstatus, parent=None):
        QMainWindow.__init__(self)

        self.ffstatus = ffstatus

        self.resize(1200, 800)
        self.init_ui()

        self.refresh()

    def init_ui(self):
        self.setWindowTitle('FFStatus')

        cw = QWidget()
        self.setCentralWidget(cw)
        layout_main = QVBoxLayout()
        layout_main.setSpacing(5)

        # toolbar
        layout_toolbar = QHBoxLayout()

        refresh_btn = QPushButton('Refresh')
        self.filter_textbox = QLineEdit('-[D] -[D,H] -[SUBETE] -[ALL]')
        filter_btn = QPushButton('Apply Filter')

        refresh_btn.pressed.connect(self.refresh)
        self.filter_textbox.returnPressed.connect(self.apply_filter)
        filter_btn.pressed.connect(self.apply_filter)

        layout_toolbar.addWidget(refresh_btn, 0)
        layout_toolbar.addWidget(QLabel('Filter:'), 0)
        layout_toolbar.addWidget(self.filter_textbox, 0)
        layout_toolbar.addWidget(filter_btn, 0)

        layout_toolbar.addStretch()
        layout_main.addLayout(layout_toolbar, 0)

        # ffstatus_window
        layout_ffstatus = QHBoxLayout()
        font = QFont('Monospace'); font.setPointSize(11); font.setStyleHint(QFont.Monospace)
        font.setStyleStrategy(QFont.NoFontMerging) # jpn characters have bigger height which desynchs scrollbars

        self.ffinfo_window = QTextEdit()
        self.ffinfo_window.setReadOnly(True)
        self.ffinfo_window.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.ffinfo_window.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.ffinfo_window.setFont(font)
        self.ffinfo_window.setLineWrapMode(False)

        self.ffstatus_window = QTextEdit()
        self.ffstatus_window.setReadOnly(True)
        self.ffstatus_window.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.ffstatus_window.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.ffstatus_window.setFont(font)
        self.ffstatus_window.setLineWrapMode(False)

        self.ffstatus_window.verticalScrollBar().valueChanged.connect(
            self._update_scroll_from_ffstatus)
        self.ffinfo_window.verticalScrollBar().valueChanged.connect(
            self._update_scroll_from_ffinfo)

        layout_ffstatus.addWidget(self.ffinfo_window, 0)
        layout_ffstatus.addWidget(self.ffstatus_window, 1)
        layout_main.addLayout(layout_ffstatus, 1)

        # add stretch
        layout_main.addStretch()

        # Setup layout
        cw.setLayout(layout_main)
        self.show()

    def _update_scroll_from_ffstatus(self, value):
        status_max = self.ffstatus_window.verticalScrollBar().maximum()
        percentage = value/float(status_max) if status_max!=0 else 0
        info_max = self.ffinfo_window.verticalScrollBar().maximum()
        self.ffinfo_window.verticalScrollBar().setValue(int(round(info_max*percentage)))

    def _update_scroll_from_ffinfo(self, value):
        info_max = self.ffinfo_window.verticalScrollBar().maximum()
        percentage = value/float(info_max) if info_max!=0 else 0
        status_max = self.ffstatus_window.verticalScrollBar().maximum()
        self.ffstatus_window.verticalScrollBar().setValue(int(round(status_max*percentage)))

    def apply_filter(self):
        self.ffstatus.apply_filter(self.filter_textbox.text())
        self.redraw_data()

    def redraw_data(self):
        ffstatus_scrollbar_value = self.ffstatus_window.verticalScrollBar().value()

        # clear
        self.ffstatus_window.clear()
        self.ffinfo_window.clear()

        # redraw data
        status_html, info_html = self.ffstatus.to_html()
        self.ffstatus_window.setHtml(status_html)
        self.ffinfo_window.setHtml(info_html)

        # resize width of info window
        info_doc_size = self.ffinfo_window.document().size()
        self.ffinfo_window.setFixedWidth(int(info_doc_size.width())+2)

        # set original scrollbar position
        self.ffstatus_window.verticalScrollBar().setValue(
            min(ffstatus_scrollbar_value, self.ffstatus_window.verticalScrollBar().maximum())
            )

    def refresh(self):
        logger.info('Refreshing...')
        self.ffstatus.reload(filter_text=self.filter_textbox.text())
        self.redraw_data()















