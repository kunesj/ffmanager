#!/usr/bin/env python3
# encoding: utf-8

import logging
import sys
import os
import argparse

from PyQt5.QtWidgets import QApplication

from ..misctools.logging_color import setup_logging
from .ffstatus import FFStatus
from .ffstatus_gui import FFStatusWindow


def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter,
        description='FFStatus\n\n'
                    'List reading status of unfinished series.'
    )
    parser.add_argument(
        '-p', '--path',
        default='./',
        help='Will check for fanfiction in given PATH and then recursively search child folders for any other '
             'fanfiction series.'
    )
    parser.add_argument(
        '--gui',
        action='store_true',
        help='Gui mode'
    )
    parser.add_argument(
        '--debug',
        action='store_true',
        help='Debug mode'
    )
    parser.add_argument(
        '--logfile',
        action='store_true',
        help='Save log to file'
    )
    args = parser.parse_args()

    setup_logging()
    _logger = logging.getLogger()

    _logger.setLevel(logging.INFO)
    if args.debug:
        _logger.setLevel(logging.DEBUG)

    _requests_logger = logging.getLogger('requests')
    if _requests_logger.getEffectiveLevel() < logging.WARNING:
        _requests_logger.setLevel(logging.WARNING)

    if args.logfile:
        try:
            os.remove('ffstatus.log')
        except OSError:
            pass
        fh = logging.FileHandler('ffmanager.log')
        fh.setFormatter(logging.Formatter(fmt='%(levelname)s:%(name)s:%(message)s'))
        _logger.addHandler(fh)

    # check if check path is dir
    ffpath = os.path.abspath(args.path)
    if os.path.isfile(ffpath):
        ffpath = os.path.dirname(ffpath)
    if not os.path.isdir(ffpath) or not os.path.exists(ffpath):
        _logger.error('path {} is not existing directory!'.format(ffpath))
        sys.exit(2)

    # create ffstatus path
    ffstatus_path = os.path.join(ffpath, 'ffstatus.ffm')

    # create ffstatus object
    ffstatus = FFStatus(ffpath=ffpath, ffstatus_path=ffstatus_path)

    if args.gui:
        app = QApplication(sys.argv)
        appwin = FFStatusWindow(ffstatus=ffstatus)
        app.exec_()
    else:
        ffstatus.reload()
        lines = ffstatus.to_plain_text()
        [print(l) for l in lines]


if __name__ == '__main__':
    main()
