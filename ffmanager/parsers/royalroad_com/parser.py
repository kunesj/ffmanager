#!/usr/bin/env python3
# coding: utf-8

import logging
from bs4 import BeautifulSoup
import datetime
from dateutil.relativedelta import relativedelta
import copy

from ..parser_base import ParserBase, dump_html_dec
from ...exceptions import ParserError

_logger = logging.getLogger(__name__)


class RoyalroadComParser(ParserBase):
    NAME = 'royalroad.com'
    DOMAINS = ['royalroad.com', 'royalroadl.com', ]

    TEMPLATE_CHAPTER = dict(ParserBase.TEMPLATE_CHAPTER, **{
        'datetime': None,
    })

    def get_base_url(self):
        return super().get_base_url().replace('royalroadl.com', 'royalroad.com')

    def get_metadata_url(self):
        return super().get_metadata_url().replace('royalroadl.com', 'royalroad.com')

    def get_ch_list_url(self):
        return super().get_ch_list_url().replace('royalroadl.com', 'royalroad.com')

    # Parsing algorithms

    def strptime(self, s):
        if s.isdigit():
            return datetime.datetime.fromtimestamp(int(s))
        else:
            num = None
            detected_period = None
            for period in ['second', 'minute', 'hour', 'day', 'month', 'year']:
                if period in s:
                    detected_period = period
                    num = s.split(period)[0].strip()
                    if num.isdigit():
                        num = int(num)
                    else:
                        raise ParserError('Could not parse datetime from "{}"'.format(s))
                    break

            if not detected_period:
                raise ParserError('Could not parse datetime from "{}"'.format(s))

            return datetime.datetime.utcnow() - relativedelta(**{detected_period+'s': num})

    @dump_html_dec
    def parse_metadata(self, html):
        soup = BeautifulSoup(html, 'lxml')
        metadata = copy.deepcopy(self.TEMPLATE_METADATA)

        # parse basic info
        metadata['url'] = self.get_metadata_url()
        metadata['name'] = soup.body.find('h1', attrs={'property': 'name'}).text.strip()
        metadata['author'] = soup.body.find('h4', attrs={'property': 'author'}).find('span', attrs={'property': 'name'}).text.strip()
        metadata['description'] = soup.body.find('div', attrs={'class': 'description'}).find('div', attrs={'property': 'description'}).text.strip()
        metadata['misc']['tags'] = ', '.join([x.text.strip() for x in soup.body.find('span', attrs={'class': 'tags'}).findAll('span')])

        # get chapter list
        chapters = self.get_chapter_list()
        metadata['chapters'] = len(chapters)
        metadata['publish_datetime'] = chapters[0]['datetime']
        metadata['update_datetime'] = chapters[-1]['datetime']

        # parse stats
        stats_div = soup.body.find('div', attrs={'class': 'stats-content'})
        rating_div = stats_div.findAll('div')[0]
        numbers_div = stats_div.findAll('div')[1]
        metadata['misc']['overall_rating'] = float(rating_div.find('meta', attrs={'property': 'ratingValue'}).get('content'))
        metadata['misc']['total_ratings'] = int(rating_div.find('meta', attrs={'property': 'ratingCount'}).get('content'))

        metadata['misc']['style_rating'] = int(rating_div.find('ul').findAll('li')[3].find('span').get('class')[1].split('star-')[-1])/10
        metadata['misc']['story_rating'] = int(rating_div.find('ul').findAll('li')[5].find('span').get('class')[1].split('star-')[-1])/10
        metadata['misc']['character_rating'] = int(rating_div.find('ul').findAll('li')[7].find('span').get('class')[1].split('star-')[-1])/10
        metadata['misc']['grammar_rating'] = int(rating_div.find('ul').findAll('li')[9].find('span').get('class')[1].split('star-')[-1])/10

        metadata['misc']['total_views'] = int(numbers_div.find('ul').findAll('li')[1].text.replace(',', ''))
        metadata['misc']['average_views'] = int(numbers_div.find('ul').findAll('li')[3].text.replace(',', ''))
        metadata['misc']['followers'] = int(numbers_div.find('ul').findAll('li')[5].text.replace(',', ''))
        metadata['misc']['favorites'] = int(numbers_div.find('ul').findAll('li')[7].text.replace(',', ''))
        metadata['misc']['pages'] = int(numbers_div.find('ul').findAll('li')[11].text.replace(',', ''))

        # fill rest of basic stats
        metadata['series'] = ['Original Work', ]

        return metadata

    @dump_html_dec
    def parse_chapter_list(self, html):
        soup = BeautifulSoup(html, 'lxml')
        chapters_table = soup.body.find('table', attrs={'id': 'chapters'})
        chapters_tr = chapters_table.find('tbody').findAll('tr')

        chapters = []
        for i, tr in enumerate(chapters_tr):
            ch = copy.deepcopy(self.TEMPLATE_CHAPTER)
            ch['number'] = i+1
            ch['name'] = tr.findAll('td')[0].find('a').text.strip()
            ch['url'] = self.get_base_url() + tr.findAll('td')[0].find('a').get('href')
            _time = tr.findAll('td')[1].find('time')
            if _time.get('unixtime'):
                ch['datetime'] = self.strptime(_time.get('unixtime'))
            else:
                ch['datetime'] = self.strptime(_time.text.strip())
            chapters.append(ch)

        return chapters

    @dump_html_dec
    def parse_chapter(self, chapter, html):
        """
        Warning! Past versions of royalroad.com had buggy html that made only part of chapter possible to parse!
        """
        soup = BeautifulSoup(html, 'lxml')
        style = 'table { border: 1px solid black; background: lightgrey; } td,th { border: 1px solid black; }'

        chapter['html'] = '''
        <html>
            <head>
                <meta http-equiv="Content-type" content="text/html; charset={}"/>
                <style>{}</style>
            </head>
            <body>
                <h2>{}</h2>
                {}
            </body>
        </html>
        '''.format(
            self.SITE_ENCODING,
            style,
            chapter['name'],
            soup.body.find('div', attrs={'class': 'chapter-inner chapter-content'}).prettify()
        ).encode(self.SITE_ENCODING)

        # TODO!!!! - use second empty <div class="chapter-inner chapter-content"></div> too check that no content was skipped

        return chapter
