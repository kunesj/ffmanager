#!/usr/bin/env python3
# coding: utf-8

import logging

from ..fanfiction_net.parser import FanfictionNetParser

_logger = logging.getLogger(__name__)


class FictionpressComParser(FanfictionNetParser):
    NAME = 'fictionpress.com'
    DOMAINS = ['fictionpress.com', ]
