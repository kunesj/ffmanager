#!/usr/bin/env python3
# coding: utf-8

import logging
from bs4 import BeautifulSoup
import datetime
import copy

from ..parser_base import ParserBase, dump_html_dec
from ...exceptions import ParserError

_logger = logging.getLogger(__name__)


class FanficsMeParser(ParserBase):
    NAME = 'fanfics.me'
    DOMAINS = ['fanfics.me', ]

    def get_base_url(self):
        return super().get_base_url().replace('https://', 'http://')

    def get_metadata_url(self):
        return super().get_metadata_url().replace('https://', 'http://')

    def get_ch_list_url(self):
        return super().get_ch_list_url().replace('https://', 'http://')

    # Parsing algorithms

    def strptime(self, s):
        return datetime.datetime.strptime(s, '%d.%m.%Y')

    @dump_html_dec
    def parse_metadata(self, html):
        soup = BeautifulSoup(html, 'lxml')
        metadata = copy.deepcopy(self.TEMPLATE_METADATA)

        # get number of chapters
        chapters = self.get_chapter_list()

        # get info
        td_first = soup.body.find('td', attrs={'class': 'first'})
        div_summary_text_fic = td_first.find('div', attrs={'class': 'summary_text_fic3'})

        metadata['chapters'] = len(chapters)
        metadata['url'] = self.get_metadata_url()
        metadata['name'] = td_first.find('h1').text.strip()
        if div_summary_text_fic:
            metadata['description'] = div_summary_text_fic.text.strip()

        for div_tr in td_first.findAll('div', attrs={'class': 'tr'}):
            title = div_tr.find('div', attrs={'class': 'title'}).text.strip().replace(':', '')
            content = div_tr.find('div', attrs={'class': 'content'}).text.strip()

            if title == 'Ссылка':
                metadata['misc']['link'] = content
            elif title == 'Автор':
                metadata['author'] = content
            elif title == 'Фандом':
                metadata['series'] = [content, ]
            elif title == 'Статус':
                metadata['misc']['status'] = content
            elif title == 'Опубликован':
                metadata['publish_datetime'] = self.strptime(content)
            elif title == 'Изменен':
                metadata['update_datetime'] = self.strptime(content)
            elif title == 'Саммари':
                metadata['description'] = content

        return metadata

    @dump_html_dec
    def parse_chapter_list(self, html):
        soup = BeautifulSoup(html, 'lxml')
        ul_content = soup.body.find('ul', attrs={'class': 'FicContents'})

        chapters = []
        i = 0
        for li in ul_content.findAll('li'):
            a = li.find('a')
            if a is None:
                continue
            if '&chapter=' not in a.get('href'):
                continue

            i += 1
            ch = copy.deepcopy(self.TEMPLATE_CHAPTER)
            ch['number'] = i
            ch['name'] = a.text.strip()
            ch['url'] = self.get_base_url()+a.get('href')
            chapters.append(ch)

        return chapters

    @dump_html_dec
    def parse_chapter(self, chapter, html):
        soup = BeautifulSoup(html, 'lxml')
        div_chapter = soup.body.find('div', attrs={'id': 'c'+str(chapter['number']-1)})

        chapter['html'] = '''
        <html>
            <head>
                <meta http-equiv="Content-type" content="text/html; charset={}"/>
            </head>
            <body>
                <h2>{}</h2>
                {}
            </body>
        </html>
        '''.format(
            self.SITE_ENCODING,
            chapter['name'],
            div_chapter.prettify()
        ).encode(self.SITE_ENCODING)

