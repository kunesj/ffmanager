#!/usr/bin/env python3
# coding: utf-8

import logging
from bs4 import BeautifulSoup
import time
import datetime
import copy

from ..parser_base import ParserBase, dump_html_dec
from ...exceptions import ParserError

_logger = logging.getLogger(__name__)


class FanfictionNetParser(ParserBase):
    NAME = 'fanfiction.net'
    DOMAINS = ['fanfiction.net', ]

    def get_html(self, url):
        html = super().get_html(url)

        soup = BeautifulSoup(html, 'lxml')
        span_gui_warning = soup.body.find('span', attrs={'class': 'gui_warning'})
        if span_gui_warning and 'Story Not Found' in span_gui_warning.text:
            raise ParserError('Not Found!')

        return html

    # Parsing algorithms

    def strptime(self, s):
        s = s.strip().lower()
        if s[-1] in ['s', 'm', 'h']:
            dt = datetime.datetime.now()
        elif len(s.split('/')) == 2:
            s += '/'+time.strftime('%Y', time.localtime())
            dt = datetime.datetime.strptime(s, '%m/%d/%Y')
        elif len(s.split('/')) == 3:
            dt = datetime.datetime.strptime(s, '%m/%d/%Y')
        else:
            raise ParserError('Unable to parse datetime string "{}"!'.format(s))
        return dt

    @dump_html_dec
    def parse_metadata(self, html):
        soup = BeautifulSoup(html, 'lxml')
        metadata = copy.deepcopy(self.TEMPLATE_METADATA)

        div_profile_top = soup.body.find('div', attrs={'id': 'profile_top'})

        # parse simple info

        metadata['url'] = self.get_metadata_url()
        metadata['name'] = div_profile_top.find('b').text.strip()
        series = soup.body.find('div', attrs={'id': 'pre_story_links'}).find('span').findAll('a')[-1].text.strip()
        if series.endswith(' Crossover'):
            metadata['series'] = [s.strip() for s in series[:-10].split(' + ')]
        else:
            metadata['series'] = [series, ]
        metadata['author'] = div_profile_top.findAll('a')[0].text.strip()
        metadata['description'] = div_profile_top.find('div', attrs={'class': 'xcontrast_txt'}).text.strip()

        # parse info string

        metadata['misc']['info_string'] = div_profile_top.find(
            'span', attrs={'class': 'xgray xcontrast_txt'}).text.strip()

        for s in metadata['misc']['info_string'].split('-'):
            s = s.strip()
            if 'Rated' in s:
                metadata['misc']['rating'] = s.split(':')[-1].strip()
                if 'T' in metadata['misc']['rating']:
                    metadata['misc']['rating'] = 'T'
                elif 'M' in metadata['misc']['rating']:
                    metadata['misc']['rating'] = 'M'
                else:  # fanfiction.net doesnt have explicit rating
                    metadata['misc']['rating'] = 'G'
            elif 'Chapters' in s:
                metadata['chapters'] = int(s.split(':')[-1].strip().replace(',', ''))
            elif 'Words' in s:
                metadata['misc']['words'] = int(s.split(':')[-1].strip().replace(',', ''))
            elif 'Reviews' in s:
                metadata['misc']['reviews'] = int(s.split(':')[-1].strip().replace(',', ''))
            elif 'Favs' in s:
                metadata['misc']['favorites'] = int(s.split(':')[-1].strip().replace(',', ''))
            elif 'Follows' in s:
                metadata['misc']['follows'] = int(s.split(':')[-1].strip().replace(',', ''))
            elif 'Updated' in s:
                metadata['update_datetime'] = self.strptime(s.split(':')[-1].strip())
            elif 'Published' in s:
                metadata['publish_datetime'] = self.strptime(s.split(':')[-1].strip())

        if not metadata.get('update_datetime'):
            metadata['update_datetime'] = metadata.get('publish_datetime')

        if not metadata.get('chapters'):
            metadata['chapters'] = 1

        return metadata

    @dump_html_dec
    def parse_chapter_list(self, html):
        soup = BeautifulSoup(html, 'lxml')
        select = soup.body.find('select', attrs={'id': 'chap_select'})

        chapters = []

        # multiple chapters
        if select is not None:
            url_parts = [
                self.get_base_url(),
                select.get('onchange').split('\'')[1],
                select.get('onchange').split('\'')[3]
            ]  # chapter_url = [0]+[1]+number+[2]
            options = select.findAll('option')

            for o in options:
                ch = copy.deepcopy(self.TEMPLATE_CHAPTER)
                ch['number'] = int(o.get('value').strip())
                ch['name'] = o.text.strip()
                ch['url'] = '{}{}{}{}'.format(url_parts[0], url_parts[1], ch['number'], url_parts[2])
                chapters.append(ch)

        # one chapter
        else:
            ch = copy.deepcopy(self.TEMPLATE_CHAPTER)
            ch['number'] = 1
            ch['name'] = 'Chapter 1'
            ch['url'] = self.get_metadata_url()
            ch['raw_html'] = html
            chapters.append(ch)

        return chapters

    @dump_html_dec
    def parse_chapter(self, chapter, html):
        soup = BeautifulSoup(html, 'lxml')

        chapter['html'] = '''
        <html>
            <head>
                <meta http-equiv="Content-type" content="text/html; charset={}"/>
            </head>
            <body>
                <h2>{}</h2>
                {}
            </body>
        </html>
        '''.format(
            self.SITE_ENCODING,
            chapter['name'],
            soup.find('div', attrs={'id': 'storytext'}).prettify()
        ).encode(self.SITE_ENCODING)

        return chapter
