#!/usr/bin/env python3
# coding: utf-8

import logging
import ssl

from ..misctools.fixed_requests import FixedRequests
from ..exceptions import ParserError

_logger = logging.getLogger(__name__)

DUMP_HTML_DEC_IS_DUMPED = False


def dump_html_dec(f):
    def wrapper(self, *args, **kwargs):
        global DUMP_HTML_DEC_IS_DUMPED
        DUMP_HTML_DEC_IS_DUMPED = False

        try:
            return f(self, *args, **kwargs)

        except Exception:
            if not DUMP_HTML_DEC_IS_DUMPED:
                _logger.error('Detected exception! Dumping html to ./ffmanager_errdump')
                DUMP_HTML_DEC_IS_DUMPED = True

                if 'html' in kwargs:
                    html = kwargs['html']
                elif 'html' in f.__code__.co_varnames:
                    html_arg_idx = f.__code__.co_varnames.index('html') - 1  # that -1 is because of 'self'
                    html = args[html_arg_idx]
                else:
                    _logger.error('Cant find html parameter! No html will be dumped!')
                    raise

                with open('ffmanager_errdump', 'w') as fp:
                    fp.write(html)

            raise

    return wrapper


class ParserBase(object):
    # Name of parser, it's usually a domain name
    NAME = None
    # website domains, used by has_url function
    DOMAINS = []

    # site encoding
    SITE_ENCODING = 'utf-8'
    # site locale (for date parsing)
    SITE_LOCALE = 'en_US.UTF-8'

    # templates
    TEMPLATE_METADATA = {'misc': {}}
    TEMPLATE_CHAPTER = {
        'number': None,  # chapter index
        'suffix': '',  # chapter id suffix
        'name': None,  # chapter name
        'url': None,  # chapter url
        'html': None,  # processed chapter text (ready to dave into html file)
        'raw_html': None  # unprocessed chapter html
    }

    # instance of FixedRequests class
    # used to keep same cookies (login sessions) between instances of same parser
    REQUESTS_INSTANCE = None

    def __init__(self, url, options=None):
        if not self.has_url(url):
            raise ParserError('Parser "{}" does not support url "{}"!'.format(self.NAME, url))
        self.url = url
        self.options = options or {}

        self.cache_metadata = None  # DEV NOTE: MUST be None
        self.cache_chapter_list = None  # DEV NOTE: MUST be None

    # Download and Connection

    @classmethod
    def get_requests(cls):
        if not cls.REQUESTS_INSTANCE:
            cls.REQUESTS_INSTANCE = FixedRequests(cloudscraper=True)
        return cls.REQUESTS_INSTANCE

    @classmethod
    def has_url(cls, url):
        """
        :param url: work url
        :return: True if parser is usable for given url
        """
        # strip protocol and www
        if url.startswith('http://') or url.startswith('https://'):
            url = '//'.join(url.split('//')[1:])

        # get domain
        domain = url.strip().split('/')[0].split('?')[0].split('#')[0]

        return any(domain.lower().endswith(d.lower()) for d in cls.DOMAINS)

    def get_base_url(self):
        # strip protocol
        if self.url.startswith('http://') or self.url.startswith('https://'):
            url = '//'.join(self.url.split('//')[1:])
        else:
            url = self.url

        # strip path after domain
        domain = url.strip().split('/')[0].split('?')[0].split('#')[0]

        return 'https://{}'.format(domain)

    def get_metadata_url(self):
        """
        :return: url of page with metadata
        """
        return self.url

    def get_ch_list_url(self):
        """
        :return: url of page with chapter list
        """
        return self.get_metadata_url()

    def get_html(self, url):
        """
        :param url: website url
        :return: unicode(html)
        """
        _logger.debug('Downloading html from: {}'.format(url))

        if not self.is_logged_in():
            self.do_login()

        r = self.get_requests().get(url=url)
        if r.status_code == 403:
            raise ParserError('HTTP 403: Forbidden (Login or CAPTCHA might be required)')
        elif r.status_code == 404:
            print(url)
            raise ParserError('HTTP 404: Not Found')
        elif r.status_code >= 400:
            raise ParserError('HTTP {}'.format(r.status_code))

        return r.text

    def is_logged_in(self):
        """
        Returns True if user is logged in
        """
        return False

    def do_login(self):
        """
        If login is required, should implement login flow.
        """
        return

    # Parsing functions

    def get_metadata(self):
        """
        :return: metadata dict
        """
        if self.cache_metadata is None:
            if not self.cache_chapter_list:
                self.get_chapter_list()

            html = self.get_html(self.get_metadata_url())
            self.cache_metadata = self.parse_metadata(html)
        return self.cache_metadata

    def get_chapter_list(self):
        """
        :return: List of self.TEMPLATE_CHAPTER
        """
        if self.cache_chapter_list is None:
            html = self.get_html(self.get_ch_list_url())
            self.cache_chapter_list = self.parse_chapter_list(html)
        return self.cache_chapter_list

    def get_chapter(self, chapter):
        """
        :param chapter: self.TEMPLATE_CHAPTER
        :return: self.TEMPLATE_CHAPTER with filled 'html'
        """
        if not chapter.get('html'):
            if chapter.get('raw_html'):
                html = chapter['raw_html']
            else:
                html = self.get_html(chapter['url'])
            self.parse_chapter(chapter, html)
        return chapter

    # Parsing algorithms

    def strptime(self, s):
        """
        Needs to be implemented by child class
        Parses site specific datetime format
        :return: datetime.datetime()
        """
        raise NotImplementedError

    @dump_html_dec
    def parse_metadata(self, html):
        """
        Needs to be implemented by child class
        :param html: unicode(html)
        :return: metadata dict
        """
        raise NotImplementedError

    @dump_html_dec
    def parse_chapter_list(self, html):
        """
        Needs to be implemented by child class
        :param html: unicode(html)
        :return: List of self.TEMPLATE_CHAPTER
        """
        raise NotImplementedError

    @dump_html_dec
    def parse_chapter(self, chapter, html):
        """
        Needs to be implemented by child class
        :param chapter: self.TEMPLATE_CHAPTER
        :param html: unicode(html)
        :return: self.TEMPLATE_CHAPTER
        """
        raise NotImplementedError
