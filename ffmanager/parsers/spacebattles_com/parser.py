#!/usr/bin/env python3
# coding: utf-8

import logging
from bs4 import BeautifulSoup

from ..xenforo2.parser import XenForo2Parser
from ...exceptions import ParserError

_logger = logging.getLogger(__name__)


class SpacebattlesComParser(XenForo2Parser):
    NAME = 'spacebattles.com'
    DOMAINS = ['spacebattles.com', ]

    def get_html(self, url):
        html = super().get_html(url)

        soup = BeautifulSoup(html, 'lxml')
        h1 = soup.body.find('h1')
        if h1 and 'Rating limit Exceeded' in h1.text:
            raise ParserError('Rating limit Exceeded!')

        return html

