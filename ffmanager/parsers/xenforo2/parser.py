#!/usr/bin/env python3
# coding: utf-8

import logging
from bs4 import BeautifulSoup
import datetime
import locale
import copy

from ..parser_base import ParserBase, dump_html_dec
from ...exceptions import ParserError

_logger = logging.getLogger(__name__)


class XenForo2Parser(ParserBase):
    # For forums powered by XenForo v2

    TEMPLATE_CHAPTER = dict(ParserBase.TEMPLATE_CHAPTER, **{
        'datetime': None,
        'author': None,
    })

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # forum_thread_mode:
        #   * threadmarks - download only threadmarks
        #   * author - download only posts made by thread author
        #   * threadmarks_and_author - download only threadmarks and posts made by thread author
        #   * all - download all posts
        self.options['forum_thread_mode'] = self.options.get('forum_thread_mode') or 'threadmarks'

    def get_metadata_url(self):
        if '/threads/' not in self.url:
            raise ParserError('Unexpected url format "{}"'.format(self.url))

        url = self.url.split('?')[0].split('#')[0]

        url_split = url.split('/threads/')
        url = url_split[0] + '/threads/' + url_split[1].split('/')[0]

        return url

    def _get_forum_page_url(self, page=1, reader=False):
        url = self.get_metadata_url()
        if url.endswith('/threadmarks'):
            url = url[:-13]

        if reader:
            url += 'reader' if url[-1] == '/' else '/reader'

        if url[-1] != '/':
            url += '/'
        url += 'page-{}'.format(page)

        return url

    # Parsing algorithms

    def strptime(self, s):
        return datetime.datetime.fromtimestamp(int(s))

    def _parse_thread_header(self, soup):
        metadata = dict()

        div_body_header = soup.body.find('div', attrs={'class': 'p-body-header'})

        metadata['name'] = div_body_header.find('h1').text.strip()
        metadata['author'] = div_body_header.find('a', attrs={'class': 'username'}).text.strip()

        return metadata

    @dump_html_dec
    def parse_metadata(self, html):
        soup = BeautifulSoup(html, 'lxml')
        metadata = copy.deepcopy(self.TEMPLATE_METADATA)

        # get chapter list and info from chapters

        chapters = self.get_chapter_list()
        if len(chapters) == 0:
            raise ParserError('No Chapters!')

        # get metadata

        header = self._parse_thread_header(soup)

        metadata['url'] = self.get_metadata_url()
        metadata['name'] = header['name']
        metadata['series'] = []
        metadata['chapters'] = len(chapters)
        metadata['author'] = header['author']
        metadata['publish_datetime'] = chapters[0]['datetime']
        metadata['update_datetime'] = chapters[-1]['datetime']

        return metadata

    @dump_html_dec
    def parse_chapter_list(self, html):
        chapter_num = 0
        page_num = 0
        reader = (self.options['forum_thread_mode'] == 'threadmarks')
        chapters = []

        while True:
            page_num += 1
            _logger.info('Processing thread page {}'.format(page_num))
            page_url = self._get_forum_page_url(page=page_num, reader=reader)

            html = self.get_html(page_url)
            soup = BeautifulSoup(html, 'lxml')

            header = self._parse_thread_header(soup)
            thread_author = header['author']

            # get raw messages

            messages = soup.body.findAll('article', attrs={'class': 'message'})

            # process messages

            for msg in messages:
                label = msg.find('div', attrs={'class': 'message-cell--threadmark-header'})
                author = msg.find('h4', attrs={'class': 'message-name'}).text.strip()

                if self.options['forum_thread_mode'] == 'threadmarks' and not label:
                    continue
                if self.options['forum_thread_mode'] == 'author' and author != thread_author:
                    continue
                if self.options['forum_thread_mode'] == 'threadmarks_and_author' and (not label and author != thread_author):
                    continue

                chapter_num += 1
                ch = copy.deepcopy(self.TEMPLATE_CHAPTER)
                ch['number'] = chapter_num
                ch['url'] = page_url+'#'+msg.get('id')
                ch['author'] = author
                ch['raw_html'] = str(msg)

                if label:
                    label_label = label.find('label')
                    label_span = label.find('span')
                    ch['name'] = '{} {}'.format(label_label.text.strip(), label_span.text.strip())
                    ch['suffix'] = label_label.text.strip()
                    if ch['suffix'][-1] == ':':
                        ch['suffix'] = ch['suffix'][:-1].lower()
                else:
                    ch['name'] = 'Chapter/Post {}'.format(chapter_num)
                    ch['suffix'] = ''
                if author != thread_author:
                    ch['name'] += ' (By: {})'.format(author)

                data_time = msg.find('header').find('time').get('data-time')
                ch['datetime'] = self.strptime(data_time)

                chapters.append(ch)

            # detect if we are on last page

            a_next_page = soup.body.find('a', attrs={'class': 'pageNav-jump--next'})
            if not a_next_page:
                break

        return chapters

    @dump_html_dec
    def parse_chapter(self, chapter, html):
        soup = BeautifulSoup(html, 'lxml')

        post_id = chapter['url'].split('#')[-1]
        soup = soup.find('article', attrs={'id': post_id})
        if not post_id or not soup:
            raise ParserError('Could not find thread post on page')

        chapter['html'] = '''
        <html>
            <head>
                <meta http-equiv="Content-type" content="text/html; charset={}"/>
            </head>
            <body>
                <h2>{}</h2>
                {}
            </body>
        </html>
        '''.format(
            self.SITE_ENCODING,
            chapter['name'],
            soup.find('div', attrs={'class': 'message-content'}).prettify()
        ).encode(self.SITE_ENCODING)

        return chapter
