#!/usr/bin/env python3
# coding: utf-8

import logging
from bs4 import BeautifulSoup
import datetime
import copy

from ..parser_base import ParserBase, dump_html_dec
from ...exceptions import ParserError

_logger = logging.getLogger(__name__)


class ArchiveofourownOrgParser(ParserBase):
    NAME = 'archiveofourown.org'
    DOMAINS = ['archiveofourown.org', ]

    def get_metadata_url(self):
        return self.url.split('?')[0] + '?view_adult=true'

    def get_html(self, url):
        html = super().get_html(url)

        soup = BeautifulSoup(html, 'lxml')

        div_inner = soup.body.find('div', attrs={'id': 'inner'})
        p_caution = div_inner.find('p', attrs={'class': 'caution'})
        if p_caution is not None:
            if p_caution.text.strip().startswith('This work could have adult content.'):
                raise ParserError('Failed to disable explicit content warning!')

        div_loginform = soup.body.find('div', attrs={'id': 'loginform'})
        if div_loginform:
            raise ParserError('Login Required!')

        div_error = soup.body.find('div', attrs={'class': 'error'})
        if div_error and 'Sorry, we couldn\'t find the work you were looking for.' in div_error.text:
            raise ParserError('Not Found!')

        return html

    # Parsing algorithms

    def strptime(self, s):
        return datetime.datetime.strptime(s.strip(), '%Y-%m-%d')

    @dump_html_dec
    def parse_metadata(self, html):
        soup = BeautifulSoup(html, 'lxml')
        metadata = copy.deepcopy(self.TEMPLATE_METADATA)

        # find divs
        div_inner = soup.body.find('div', attrs={'id': 'inner'})
        div_wrapper = div_inner.find('div', attrs={'class': 'wrapper'})  # fanfiction information
        div_workskin = div_inner.find('div', attrs={'id': 'workskin'})
        div_preface = div_workskin.find('div', attrs={'class': 'preface group'})  # name of series, description
        div_chapters = div_workskin.find('div', attrs={'id': 'chapters'})  # text of chapter

        # parse basic info

        metadata['url'] = self.get_metadata_url()
        metadata['name'] = div_preface.find('h2').text.strip()
        metadata['author'] = div_preface.find('h3').text.strip()
        if div_preface.find('blockquote', attrs={'class': 'userstuff'}) is not None:
            metadata['description'] = div_preface.find('blockquote', attrs={'class': 'userstuff'}).text.strip()
        else:
            metadata['description'] = ''
        metadata['series'] = []
        for li in div_wrapper.find('dd', attrs={'class': 'fandom tags'}).findAll('li'):
            metadata['series'].append(li.text.strip())

        # misc info

        metadata['misc']['rating'] = div_wrapper.find('dd', attrs={'class': 'rating tags'}).text.strip()
        if metadata['misc']['rating'].startswith('General'):
            metadata['misc']['rating'] = 'G'
        elif metadata['misc']['rating'].startswith('Teen'):
            metadata['misc']['rating'] = 'T'
        elif metadata['misc']['rating'].startswith('Mature'):
            metadata['misc']['rating'] = 'M'
        elif metadata['misc']['rating'].startswith('Explicit'):
            metadata['misc']['rating'] = 'E'
        else:  # metadata['misc']['rating'].startswith('Not')
            metadata['misc']['rating'] = 'N'

        metadata['misc']['warning'] = div_wrapper.find('dd', attrs={'class': 'warning tags'}).text.strip()
        if div_wrapper.find('dd', attrs={'class': 'category tags'}) is not None:
            metadata['misc']['category'] = div_wrapper.find('dd', attrs={'class': 'category tags'}).text.strip()
        else:
            metadata['misc']['category'] = ''
        if div_wrapper.find('dd', attrs={'class': 'character tags'}) is not None:
            metadata['misc']['character'] = div_wrapper.find('dd', attrs={'class': 'character tags'}).text.strip()
        else:
            metadata['misc']['character'] = ''
        if div_wrapper.find('dd', attrs={'class': 'relationship tags'}) is not None:
            metadata['misc']['relationship'] = div_wrapper.find('dd', attrs={'class': 'relationship tags'}).text.strip()
        else:
            metadata['misc']['relationship'] = ''
        if div_wrapper.find('dd', attrs={'class': 'freeform tags'}) is not None:
            metadata['misc']['freeform'] = div_wrapper.find('dd', attrs={'class': 'freeform tags'}).text.strip()
        else:
            metadata['misc']['freeform'] = ''
        metadata['misc']['language'] = div_wrapper.find('dd', attrs={'class': 'language'}).text.strip()

        # parse stats tags

        dl_stats = div_wrapper.find('dl', attrs={'class': 'stats'})
        metadata['misc']['stats'] = dl_stats.text.strip()
        stats_dt = dl_stats.findAll('dt')
        stats_dd = dl_stats.findAll('dd')

        metadata['misc']['reviews'] = 0
        metadata['misc']['favorites'] = 0
        metadata['misc']['follows'] = 0
        for i, dt in enumerate(stats_dt):
            label = dt.text.strip().replace(':', '')
            if label == 'Published':
                metadata['publish_datetime'] = self.strptime(stats_dd[i].text.strip())
            elif label == 'Updated':
                metadata['update_datetime'] = self.strptime(stats_dd[i].text.strip())
            elif label == 'Completed':  # replaces 'Updated' label
                metadata['update_datetime'] = self.strptime(stats_dd[i].text.strip())
            elif label == 'Words':
                metadata['misc']['words'] = int(stats_dd[i].text.strip())
            elif label == 'Chapters':
                metadata['chapters'] = int(stats_dd[i].text.strip().split('/')[0])
            elif label == 'Comments':
                metadata['misc']['reviews'] = int(stats_dd[i].text.strip())
            elif label == 'Kudos':
                metadata['misc']['favorites'] = int(stats_dd[i].text.strip())
            elif label == 'Bookmarks':
                metadata['misc']['follows'] = int(stats_dd[i].text.strip())
            # elif label == 'Hits':
            #     metadata['misc']['hits'] = int(stats_dd[i].text.strip())

        if not metadata.get('update_datetime'):
            metadata['update_datetime'] = metadata.get('publish_datetime')

        return metadata

    @dump_html_dec
    def parse_chapter_list(self, html):
        soup = BeautifulSoup(html, 'lxml')
        chapters = []

        ul_ch_index = soup.body.find('ul', attrs={'id': 'chapter_index'})

        # onc chapter

        if ul_ch_index is None:
            ch = copy.deepcopy(self.TEMPLATE_CHAPTER)
            ch['number'] = 1
            ch['name'] = 'Chapter 1'
            ch['url'] = self.get_metadata_url()
            ch['raw_html'] = html
            chapters.append(ch)
            return chapters

        # multiple chapters

        options = ul_ch_index.findAll('option')
        work_value = ul_ch_index.find('form').get('action').split('/')[2]

        for i, o in enumerate(options):
            ch = copy.deepcopy(self.TEMPLATE_CHAPTER)
            ch['number'] = i+1
            ch['name'] = o.text.strip()
            ch['url'] = '{}/works/{}/chapters/{}'.format(self.get_base_url(), work_value, o.get('value'))
            chapters.append(ch)

        return chapters

    @dump_html_dec
    def parse_chapter(self, chapter, html):
        soup = BeautifulSoup(html, 'lxml')

        div_inner = soup.body.find('div', attrs={'id': 'inner'})
        div_workskin = div_inner.find('div', attrs={'id': 'workskin'})
        div_chapters = div_workskin.find('div', attrs={'id': 'chapters'})  # text of chapter

        chapter['html'] = '''
        <html>
            <head>
                <meta http-equiv="Content-type" content="text/html; charset={}"/>
            </head>
            <body>
                {}
            </body>
        </html>
        '''.format(
            self.SITE_ENCODING,
            div_chapters.prettify()
        ).encode(self.SITE_ENCODING)

        return chapter
