#!/usr/bin/env python3
# coding: utf-8

from ..misctools.import_submodules import import_submodules
from ..exceptions import ParserError
from .parser_base import ParserBase

# make sure all parsers are imported
import_submodules(__name__)


def get_all_subclasses(cls):
    all_subclasses = []
    for subclass in cls.__subclasses__():
        all_subclasses.append(subclass)
        all_subclasses.extend(get_all_subclasses(subclass))
    return list(set(all_subclasses))


def get_parser_class(search_string):
    if '/' in search_string:
        return get_parser_class_by_url(search_string)
    else:
        return get_parser_class_by_name(search_string)


def get_parser_class_by_url(url):
    for parser in get_all_subclasses(ParserBase):
        if parser.has_url(url):
            return parser
    return None


def get_parser_class_by_name(name):
    for parser in get_all_subclasses(ParserBase):
        if parser.NAME == name:
            return parser
    return None


def init_parser(url, options):
    if not url:
        raise ParserError('Can\'t find parser without url!')
    parser_class = get_parser_class(url)
    if not parser_class:
        raise ParserError('Could not find parser for url "{}"!'.format(url))
    return parser_class(url, options)
