#!/usr/bin/env python3
# coding: utf-8

import logging

from ..xenforo2.parser import XenForo2Parser

_logger = logging.getLogger(__name__)


class SufficientvelocityComParser(XenForo2Parser):
    NAME = 'sufficientvelocity.com'
    DOMAINS = ['sufficientvelocity.com', ]

