#!/usr/bin/env python3
# coding: utf-8

import logging
from bs4 import BeautifulSoup
import datetime
import copy

from ..parser_base import ParserBase, dump_html_dec
from ...exceptions import ParserError

_logger = logging.getLogger(__name__)


class AdultfanfictionOrgParser(ParserBase):
    NAME = 'adult-fanfiction.org'
    DOMAINS = ['adult-fanfiction.org', ]

    def get_base_url(self):
        return super().get_base_url().replace('https://', 'http://')

    def get_metadata_url(self):
        return super().get_metadata_url().replace('https://', 'http://')

    def get_ch_list_url(self):
        return super().get_ch_list_url().replace('https://', 'http://')

    # Parsing algorithms

    def strptime(self, s):
        raise NotImplementedError

    @dump_html_dec
    def parse_metadata(self, html):
        soup = BeautifulSoup(html, 'lxml')
        metadata = copy.deepcopy(self.TEMPLATE_METADATA)

        # get chapter list

        chapters = self.get_chapter_list()

        # get general info

        div_contentdata = soup.body.find('div', attrs={'id': 'contentdata'})
        info_table = div_contentdata.find('table').find('table')
        info_tr = info_table.find('tr')
        info_i = info_tr.findAll('td')[1].find('i')

        metadata['url'] = self.get_metadata_url()
        metadata['chapters'] = len(chapters)
        metadata['name'] = info_tr.findAll('td')[0].text.strip()
        metadata['author'] = info_i.findAll('a')[0].text.strip()
        metadata['series'] = []
        for a in info_i.findAll('a')[1:]:
            if a.text.strip().startswith('+') or a.text.strip().startswith('-'):
                continue
            metadata['series'].append(a.text.strip())
        metadata['misc']['dragon_prints'] = int(info_i.text.split('Dragon prints:')[-1].strip())

        return metadata

    @dump_html_dec
    def parse_chapter_list(self, html):
        soup = BeautifulSoup(html, 'lxml')
        div_dropdown = soup.body.find('div', attrs={'class': 'dropdown-content'})

        chapters = []
        for i, a in enumerate(div_dropdown.findAll('a')):
            ch = copy.deepcopy(self.TEMPLATE_CHAPTER)
            ch['number'] = i+1
            ch['name'] = a.text.strip()
            ch['url'] = self.get_base_url()+a.get('href')
            chapters.append(ch)

        return chapters

    @dump_html_dec
    def parse_chapter(self, chapter, html):
        soup = BeautifulSoup(html, 'lxml')

        div_contentdata = soup.body.find('div', attrs={'id': 'contentdata'})
        trs = div_contentdata.find('table').findAll('tr')

        chapter['html'] = '''
        <html>
            <head>
                <meta http-equiv="Content-type" content="text/html; charset={}"/>
            </head>
            <body>
                {}
            </body>
        </html>
        '''.format(
            self.SITE_ENCODING,
            trs[5].find("td").prettify()
        ).encode(self.SITE_ENCODING)

        return chapter
