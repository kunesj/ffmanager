#!/usr/bin/env python3
# coding: utf-8

import logging
from bs4 import BeautifulSoup
import datetime
import locale
import copy

from ..parser_base import ParserBase, dump_html_dec
from ...exceptions import ParserError

_logger = logging.getLogger(__name__)


class XenForoParser(ParserBase):
    # For forums powered by XenForo v1

    TEMPLATE_CHAPTER = dict(ParserBase.TEMPLATE_CHAPTER, **{
        'datetime': None,
        'author': None,
    })

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # forum_thread_mode:
        #   * threadmarks - download only threadmarks
        #   * author - download only posts made by thread author
        #   * threadmarks_and_author - download only threadmarks and posts made by thread author
        #   * all - download all posts
        self.options['forum_thread_mode'] = self.options.get('forum_thread_mode') or 'threadmarks'

    def get_metadata_url(self):
        if '/threads/' not in self.url:
            raise ParserError('Unexpected url format "{}"'.format(self.url))

        url = self.url.split('?')[0].split('#')[0]

        url_split = url.split('/threads/')
        url = url_split[0] + '/threads/' + url_split[1].split('/')[0]

        return url

    def _get_forum_page_url(self, page=1, reader=False):
        url = self.get_metadata_url()
        if url.endswith('/threadmarks'):
            url = url[:-13]

        if reader:
            url += 'reader' if url[-1] == '/' else '/reader'

        if url[-1] != '/':
            url += '/'
        url += '?page={}'.format(page)

        return url

    # Parsing algorithms

    def strptime(self, s):
        locale.setlocale(locale.LC_ALL, self.SITE_LOCALE)
        return datetime.datetime.strptime(s, '%b %d, %Y')

    def _parse_thread_header(self, soup):
        metadata = dict()

        div_titlebar = soup.body.find('div', attrs={'class': 'titleBar'})

        metadata['name'] = div_titlebar.find('h1').text.strip()
        metadata['author'] = div_titlebar.find('p').find('a', attrs={'class': 'username'}).text.strip()

        return metadata

    @dump_html_dec
    def parse_metadata(self, html):
        soup = BeautifulSoup(html, 'lxml')
        metadata = copy.deepcopy(self.TEMPLATE_METADATA)

        # get chapter list and info from chapters

        chapters = self.get_chapter_list()
        if len(chapters) == 0:
            raise ParserError('No Chapters!')

        # get metadata

        header = self._parse_thread_header(soup)

        metadata['url'] = self.get_metadata_url()
        metadata['name'] = header['name']
        metadata['series'] = []
        metadata['chapters'] = len(chapters)
        metadata['author'] = header['author']
        metadata['publish_datetime'] = chapters[0]['datetime']
        metadata['update_datetime'] = chapters[-1]['datetime']

        return metadata

    @dump_html_dec
    def parse_chapter_list(self, html):  # TODO: other threadmark categories (in reader mode) (maybe use RSS??)
        chapter_num = 0
        page_num = 0
        reader = (self.options['forum_thread_mode'] == 'threadmarks')
        chapters = []

        while True:
            page_num += 1
            _logger.info('Processing thread page {}'.format(page_num))
            page_url = self._get_forum_page_url(page=page_num, reader=reader)

            html = self.get_html(page_url)
            soup = BeautifulSoup(html, 'lxml')

            header = self._parse_thread_header(soup)
            thread_author = header['author']

            # get raw messages

            ol_messageList = soup.body.find('ol', attrs={'id': 'messageList'})
            lis = ol_messageList.findAll('li', recursive=False)

            # process messages

            for li in lis:
                label = li.find('span', attrs={'class': 'label'})
                div_private_controls = li.find('div', attrs={'class': 'privateControls'})
                author = div_private_controls.find('span', attrs={'class': 'authorEnd'}).find('a').text.strip()

                if self.options['forum_thread_mode'] == 'threadmarks' and not label:
                    continue
                if self.options['forum_thread_mode'] == 'author' and author != thread_author:
                    continue
                if self.options['forum_thread_mode'] == 'threadmarks_and_author' and (not label and author != thread_author):
                    continue

                chapter_num += 1
                ch = copy.deepcopy(self.TEMPLATE_CHAPTER)
                ch['number'] = chapter_num
                ch['url'] = page_url+'#'+li.get('id')
                ch['author'] = author
                ch['raw_html'] = str(li)

                if label:
                    ch['name'] = label.text.strip()
                    ch['suffix'] = label.find('strong').text.strip()
                    if ch['suffix'][-1] == ':':
                        ch['suffix'] = ch['suffix'][:-1].lower()
                else:
                    ch['name'] = 'Chapter/Post {}'.format(chapter_num)
                    ch['suffix'] = ''
                if author != thread_author:
                    ch['name'] += ' (By: {})'.format(author)

                span_dt = div_private_controls.find('span', attrs={'class': 'DateTime'})
                abbr_dt = div_private_controls.find('abbr', attrs={'class': 'DateTime'})
                if span_dt:
                    ch['datetime'] = self.strptime(span_dt.text.strip())
                elif abbr_dt:
                    ch['datetime'] = self.strptime(abbr_dt['data-datestring'])
                else:
                    raise ParserError('Could not parse datetime for chapter {} "{}"'.format(ch['number'], ch['name']))

                chapters.append(ch)

            # detect if we are on last page

            a_list = soup.body.findAll('a', attrs={'class': 'text'})
            if not any('Next >' in a.text for a in a_list):
                break

        return chapters

    @dump_html_dec
    def parse_chapter(self, chapter, html):
        soup = BeautifulSoup(html, 'lxml')

        post_id = chapter['url'].split('#')[-1]
        soup = soup.find('li', attrs={'id': post_id})
        if not post_id or not soup:
            raise ParserError('Could not find thread post on page')

        chapter['html'] = '''
        <html>
            <head>
                <meta http-equiv="Content-type" content="text/html; charset={}"/>
            </head>
            <body>
                <h2>{}</h2>
                {}
            </body>
        </html>
        '''.format(
            self.SITE_ENCODING,
            chapter['name'],
            soup.find('div', attrs={'class': 'messageContent'}).prettify()
        ).encode(self.SITE_ENCODING)

        return chapter
