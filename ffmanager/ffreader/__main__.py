#!/usr/bin/env python3
# encoding: utf-8

import os
import sys
import argparse
import traceback
import natsort

from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import QMainWindow, QApplication, QVBoxLayout, QTextEdit, QPushButton, \
    QWidget, QLabel, QHBoxLayout, QLineEdit, QFileDialog

import qdarkstyle  # dark qt theme


class ReaderWindow(QMainWindow):
    # keypress signals
    keyPressed_zoomin = QtCore.pyqtSignal()
    keyPressed_zoomout = QtCore.pyqtSignal()
    keyPressed_zoomreset = QtCore.pyqtSignal()
    keyPressed_skinchange = QtCore.pyqtSignal()
    keyPressed_maximize = QtCore.pyqtSignal()
    keyPressed_search = QtCore.pyqtSignal()
    keyPressed_lastfile = QtCore.pyqtSignal()
    keyPressed_nextfile = QtCore.pyqtSignal()
    keyPressed_browse = QtCore.pyqtSignal()

    # Skins [ [text, back, dark], [text, back, dark], ... ]
    skins = [
        [(0, 0, 0), (255, 255, 255), False],
        [(18, 18, 18), (225, 225, 225), False],
        [(18, 18, 18), (195, 195, 195), False],
        [(195, 195, 195), (28, 28, 28), True],
        [(255, 255, 255), (0, 0, 0), True],
    ]

    def __init__(self, parent=None, path='./'):
        QMainWindow.__init__(self)

        self.resize(700, 500)
        self.initUI()

        self.zoom_value = 0

        self.directory = None
        self.last_file = None
        self.this_file = None
        self.next_file = None

        self.default_style_sheet = self.styleSheet()
        self.skin_num = 0
        self.skin_switch(0)

        self.show_search = False
        self.search_switch(False)

        self.open_path(path)

    def initUI(self):
        cw = QWidget()
        self.setCentralWidget(cw)
        layout_main = QVBoxLayout()
        layout_main.setSpacing(5)

        self.setWindowTitle('FFReader')

        # Directory path bar
        dirbar_l = QHBoxLayout()

        dirbar_l.addWidget(QLabel('<b>DIR:</b>'), 0)

        self.directory_label = QLabel('---')
        self.directory_label.setWordWrap(True)
        dirbar_l.addWidget(self.directory_label, 1)

        browse_btn = QPushButton('Browse [B]')
        browse_btn.pressed.connect(self.browse_dir)
        dirbar_l.addWidget(browse_btn, 0)

        layout_main.addLayout(dirbar_l)

        # File Selector bar
        filebar_l = QHBoxLayout()

        last_l = QHBoxLayout()
        last_l.addWidget(QLabel('<b>Last [Q]:</b>'), 0)
        self.last_file_label = QLabel('---')
        last_l.addWidget(self.last_file_label, 1)
        filebar_l.addLayout(last_l, 1)

        this_l = QHBoxLayout()
        this_l.addWidget(QLabel('<b>This:</b>'), 0)
        self.this_file_label = QLabel('---')
        this_l.addWidget(self.this_file_label, 1)
        filebar_l.addLayout(this_l, 1)

        next_l = QHBoxLayout()
        next_l.addWidget(QLabel('<b>Next [W]:</b>'), 0)
        self.next_file_label = QLabel('---')
        next_l.addWidget(self.next_file_label, 1)
        filebar_l.addLayout(next_l, 1)

        layout_main.addLayout(filebar_l)

        # HTML widget
        self.htmlwindow = QTextEdit()
        self.htmlwindow.setReadOnly(True)
        self.htmlwindow.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.htmlwindow.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)

        layout_main.addWidget(self.htmlwindow, 1)

        # Search bar
        searchbar_l = QHBoxLayout()

        searchbar_l.addWidget(QLabel('<b>Search:</b>'), 0)

        self.search_edit = QLineEdit()
        self.search_edit.returnPressed.connect(self.search_process)
        searchbar_l.addWidget(self.search_edit, 1)

        search_btn = QPushButton('Search [Return]')
        search_btn.pressed.connect(self.search_process)
        searchbar_l.addWidget(search_btn, 0)

        search_esc_btn = QPushButton('Close [Esc]')
        search_esc_btn.pressed.connect(self.search_close)
        searchbar_l.addWidget(search_esc_btn, 0)

        self.searchbar_w = QWidget()
        self.searchbar_w.setLayout(searchbar_l)
        layout_main.addWidget(self.searchbar_w)

        # Key Shortcuts + Help bar
        helpbar = QLabel('[I] zoom in; [O] zoom out; [P] zoom reset; [N] change skin (night mode); [M] maximize; [F] enable search;')
        helpbar.setWordWrap(True)
        layout_main.addWidget(helpbar, 0)

        self.keyPressed_zoomin.connect(self.zoom_in)
        self.keyPressed_zoomout.connect(self.zoom_out)
        self.keyPressed_zoomreset.connect(self.zoom_reset)
        self.keyPressed_skinchange.connect(self.skin_switch)
        self.keyPressed_maximize.connect(self.maximize)
        self.keyPressed_search.connect(self.search_switch)
        self.keyPressed_lastfile.connect(self.load_last_file)
        self.keyPressed_nextfile.connect(self.load_next_file)
        self.keyPressed_browse.connect(self.browse_dir)

        # add stretch
        layout_main.addStretch()

        # Setup layout
        cw.setLayout(layout_main)
        self.show()

    def keyPressEvent(self, event):
        super(ReaderWindow, self).keyPressEvent(event)

        if event.key() == QtCore.Qt.Key_Escape:
            if self.show_search is True:
                self.search_close()

        elif event.key() == QtCore.Qt.Key_I:
            self.keyPressed_zoomin.emit()

        elif event.key() == QtCore.Qt.Key_O:
            self.keyPressed_zoomout.emit()

        elif event.key() == QtCore.Qt.Key_P:
            self.keyPressed_zoomreset.emit()

        elif event.key() == QtCore.Qt.Key_N:
            self.keyPressed_skinchange.emit()

        elif event.key() == QtCore.Qt.Key_F:
            self.keyPressed_search.emit()

        elif event.key() == QtCore.Qt.Key_M:
            self.keyPressed_maximize.emit()

        elif event.key() == QtCore.Qt.Key_Q:
            self.keyPressed_lastfile.emit()

        elif event.key() == QtCore.Qt.Key_W:
            self.keyPressed_nextfile.emit()

        elif event.key() == QtCore.Qt.Key_B:
            self.keyPressed_browse.emit()

    # Event functions

    def zoom_in(self):
        self.htmlwindow.zoomIn(2)
        self.zoom_value += 2

    def zoom_out(self):
        self.htmlwindow.zoomOut(2)
        self.zoom_value -= 2

    def zoom_reset(self):
        if self.zoom_value > 0:
            self.htmlwindow.zoomOut(self.zoom_value)
        elif self.zoom_value < 0:
            self.htmlwindow.zoomIn(-self.zoom_value)
        self.zoom_value = 0

    def skin_switch(self, new_num = None):
        # get new skin number to use
        if new_num is not None:
            new_num = self.skin_num
        elif self.skin_num >= len(self.skins)-1:
            new_num = 0
        else:
            new_num = self.skin_num + 1

        # change qt stylesheet
        if self.skins[new_num][2]:
            self.setStyleSheet(qdarkstyle.load_stylesheet_pyqt5())
        else:
            self.setStyleSheet(self.default_style_sheet)

        # change htmlwindow theme
        self.htmlwindow.setStyleSheet(\
            'QTextEdit{ color: rgb(%i,%i,%i); background-color: rgb(%i,%i,%i); }' % \
            tuple(self.skins[new_num][0]+self.skins[new_num][1]) )

        # save current skin num
        self.skin_num = new_num

    def maximize(self):
        self.showMaximized()

    def search_switch(self, new_val = None):
        if new_val is None:
            new_val = not self.show_search

        if new_val is True:
            self.searchbar_w.setHidden(False)
            self.search_edit.setEnabled(True)
            self.search_edit.setFocus()
        else:
            self.searchbar_w.setHidden(True)
            self.search_edit.setEnabled(False)

        self.show_search = new_val

    def search_close(self):
        self.search_switch(False)

    def search_process(self):
        pattern = self.search_edit.text()
        self.htmlwindow.find(pattern)

    def load_last_file(self):
        self.update_filenames()
        if self.last_file is not None:
            self.load_file(self.last_file)

    def load_next_file(self):
        self.update_filenames()
        if self.next_file is not None:
            self.load_file(self.next_file)

    def browse_dir(self):
        if self.directory is not None and os.path.exists(self.directory) and os.path.isdir(self.directory):
            old_dir = self.directory
        else:
            old_dir = os.path.abspath('./')

        new_dir = '%s' % QFileDialog.getOpenFileName(
            caption='Select Folder',
            directory=old_dir
        )[0]
        if len(new_dir) == 0:
            return  # browsing cenceled

        new_dir = self.decode_path(new_dir)
        try:
            self.open_path(new_dir)
        except Exception:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            trace = traceback.format_exception(exc_type, exc_value, exc_traceback)
            msg = 'Error when opening path: %s<br />%s' % (new_dir, '<br />'.join(trace))
            print(msg)
            self.load_blank(msg=msg)

    # OS functions

    def decode_path(self, path):
        """ Will try to return path as unicode string. Throws exception if error. """
        try:
            # for conversion from qtstring
            path = str(path)
        except Exception:
            # for conversion from path given by OS
            path = path.decode(sys.getfilesystemencoding())
        return path

    def open_path(self, path):
        path = os.path.abspath(self.decode_path(path))
        if os.path.isdir(path):
            self.directory = path
        elif os.path.isfile(path):
            self.directory = os.path.dirname(path)
        else:
            raise Exception('Path is not directory or file!!!')

        # set directory_bar value
        self.directory_label.setText(self.directory)

        # reset file values
        self.last_file = None
        self.this_file = None
        self.next_file = None

        # load file
        if os.path.isfile(path):
            self.load_file(os.path.basename(path))
        else:
            fl = self.get_filelist()
            if len(fl) != 0:
                self.load_file(fl[0])
            else:
                self.load_blank()

    def get_filelist(self, dirpath=None):
        if dirpath is None:
            dirpath = self.directory
        onlyfiles = [ f for f in os.listdir(dirpath) if os.path.isfile(os.path.join(dirpath, f)) ]
        onlyhtmlfiles = [ f for f in onlyfiles if f.split('.')[-1] in ['html','htm'] ]
        onlyhtmlfiles = natsort.natsorted(onlyhtmlfiles)
        return onlyhtmlfiles

    def update_filenames(self):
        try:
            filelist = self.get_filelist(self.directory)

            # get index of current file
            if self.this_file is None:
                file_index = -1
            else:
                try:
                    file_index = filelist.index(self.this_file)
                except ValueError:
                    print('File is not HTML file??? (%s)' % self.this_file)
                    file_index = -1

            # get next/last filenames
            if file_index == -1:
                self.last_file = None
                self.next_file = filelist[0] if len(filelist)>0 else None
            elif file_index == 0:
                self.last_file = None
                self.next_file = filelist[file_index+1] if len(filelist)>1 else None
            elif file_index == len(filelist)-1:
                self.last_file = filelist[file_index-1]
                self.next_file = None
            else:
                self.last_file = filelist[file_index-1]
                self.next_file = filelist[file_index+1]
            self.update_filebar() # update gui labels

        except Exception:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            trace = traceback.format_exception(exc_type, exc_value, exc_traceback)
            msg = 'Error when updating filenames!<br />%s' % '<br />'.join(trace)
            print(msg)
            self.load_blank(msg=msg)

    def load_blank(self, msg=None):
        if msg is None:
            msg = 'No HTML files (*.html, *.htm) found in current directory'

        self.last_file = None
        self.this_file = None
        self.next_file = None
        self.update_filebar() # update gui labels

        html_data = '<h2>'+msg+'</h2>'
        self.load_html(html_data)

    def load_file(self, basename):
        try:
            file_path = os.path.join(self.directory, basename)
            if not os.path.isfile(file_path):
                raise Exception('Could not find file: '+file_path)

            # update filenames
            self.this_file = basename
            self.update_filenames()

            # load html from file
            with open(file_path, 'r') as f:
                self.load_html(f.read(), plain_text=(file_path.split('.')[-1] not in ['html','htm']))

        except Exception:
            exc_type, exc_value, exc_traceback = sys.exc_info()
            trace = traceback.format_exception(exc_type, exc_value, exc_traceback)
            msg = 'Error when opening file: %s<br />%s' % (basename, '<br />'.join(trace))
            print(msg)
            self.load_blank(msg=msg)

    def load_html(self, html, plain_text=False):
        if not plain_text:
            # dirty fix for horizontal tearing in loaded html. (first draw ignores top-padding of fist line)
            html = '<p style="margin:0px;padding:0px;font-size:1px;"></p>%s' % html

        self.html_data = html
        if plain_text:
            self.htmlwindow.setText(self.html_data)
        else:
            self.htmlwindow.setHtml(self.html_data)
        self.htmlwindow.verticalScrollBar().setSliderPosition(0)

    def update_filebar(self):
        if self.last_file is None:
            self.last_file_label.setText('---')
        else:
            self.last_file_label.setText(self.last_file)

        if self.this_file is None:
            self.this_file_label.setText('---')
        else:
            self.this_file_label.setText(self.this_file)

        if self.next_file is None:
            self.next_file_label.setText('---')
        else:
            self.next_file_label.setText(self.next_file)


def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter,
        description='FFreader \n\n'
                    'Read downloaded (very simple) HTML files. Must be encoded in UTF-8 (or just ASCII).\n\n'
                    'Shortcuts:\n'
                    '  i - zoom in\n'
                    '  o - zoom out\n'
                    '  p - zoom reset\n'
                    '  n - change skin (night mode)\n'
                    '  m - maximize window\n'
                    '  f - enable search\n'
                    '  q - load last file in directory\n'
                    '  w - load next file in directory\n'
                    '  b - browse dir'
    )
    parser.add_argument(
        'path',
        nargs='?',
        default='./',
        help='Path to HTML file or directory with files to read')
    args = parser.parse_args()

    if not os.path.exists(args.path):
        print('%s is not existing path!!!' % (args.path,))
        sys.exit(2)

    app = QApplication(sys.argv)
    appwin = ReaderWindow(path=args.path)
    app.exec_()


if __name__ == '__main__':
    main()
