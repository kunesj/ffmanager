#!/usr/bin/env python3
# coding: utf-8

import logging
import os
import json
import datetime
import copy
import tempfile

from .exceptions import FFManagerError
from .parsers import init_parser

_logger = logging.getLogger(__name__)


class Work(object):

    FFINFO_FILENAME = 'ffinfo.ffm'
    DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'

    CHAPTER_EXTENSION_BLACKLIST = [
        'ffm', 'png', 'jpg', 'jpeg', 'bmp', 'svg',
    ]

    def __init__(self, database_path, path=None):
        """
        :param database_path: Path to folder with works (this work should be located under this folder)
        :param path: Path to work or it's ffinfo file
        """
        database_path = os.path.dirname(database_path) if os.path.isfile(database_path) else database_path
        path = os.path.dirname(path) if (path and os.path.isfile(path)) else path

        self.database_path = os.path.abspath(database_path)
        self.path = os.path.abspath(path) if path else None

        if self.database_path == self.path:
            self.database_path = os.path.dirname(os.path.normpath(self.database_path))

        # metadata

        self.metadata = {}

        self.id = None
        self.name = None
        self.series = []

        self.publish_datetime = None  # datetime of  first published chapter
        self.update_datetime = None  # datetime of last published chapter
        self.download_datetime = None  # datetime of last metadata download

        self.url = None
        self.options = {}  # download options

        self.author = None
        self.chapters = None
        self.description = None
        self.related_works = []  # [('work_id', 'relation type'), ...]

        self.misc = {}  # misc metadata

        # load metadata

        self.load_metadata()

    @classmethod
    def is_work(cls, path):
        return os.path.exists(cls.get_ffinfo_path(path))

    # metadata + ffinfo

    def load_metadata(self, metadata=None):
        # load ffinfo

        self.metadata = {}
        if metadata:
            self.metadata = metadata
        elif self.path and os.path.exists(self.get_ffinfo_path(self.path)):
            self.metadata = self.read_ffinfo(self.path)

        # parse datetime strings

        if 'publish_datetime' in self.metadata:
            self.metadata['publish_datetime'] = self.strptime(self.metadata['publish_datetime'])
        if 'update_datetime' in self.metadata:
            self.metadata['update_datetime'] = self.strptime(self.metadata['update_datetime'])
        if 'download_datetime' in self.metadata:
            self.metadata['download_datetime'] = self.strptime(self.metadata['download_datetime'])

        # parse important values

        self.id = self.metadata.get('id')
        self.name = self.metadata.get('name')
        self.series = self.metadata.get('series') or []

        self.url = self.metadata.get('url')
        self.options = self.metadata.get('options') or {}

        self.publish_datetime = self.metadata.get('publish_datetime')
        self.update_datetime = self.metadata.get('update_datetime')
        self.download_datetime = self.metadata.get('download_datetime')

        self.author = self.metadata.get('author')
        self.chapters = self.metadata.get('chapters')
        self.description = self.metadata.get('description')
        self.related_works = self.metadata.get('related_works') or []

        self.misc = self.metadata.get('misc') or {}

        # build work path

        if not self.path and self.id:
            self.path = os.path.join(self.database_path, self.id)

    def save_metadata(self, write_to_file=True):
        # copy important values to metadata

        self.metadata['id'] = self.id
        self.metadata['name'] = self.name
        self.metadata['series'] = self.series

        self.metadata['url'] = self.url
        self.metadata['options'] = self.options

        self.metadata['publish_datetime'] = self.publish_datetime
        self.metadata['update_datetime'] = self.update_datetime
        self.metadata['download_datetime'] = self.download_datetime

        self.metadata['author'] = self.author
        self.metadata['chapters'] = self.chapters
        self.metadata['description'] = self.description
        self.metadata['related_works'] = self.related_works

        self.metadata['misc'] = self.misc

        # save ffinfo

        if write_to_file:
            if not self.path:
                raise FFManagerError('Work path not defined!')
            self.write_ffinfo(self.metadata, self.path)

        return self.metadata

    # @classmethod
    # def get_metadata_template(cls):  # TODO: not used
    #     return cls(tempfile.gettempdir()).save_metadata(write_to_file=False)

    @classmethod
    def get_ffinfo_path(cls, path, mkdir=False, must_exist=False):
        """
        :param path: can be path directly to ffinfo file or just to work folder
        :param mkdir: create directory with ffinfo file if it doesnt exist
        :param must_exist: raise exception if ffinfo file does not exist
        :return: path to ffinfo file
        """
        path = os.path.abspath(path)
        if not os.path.isfile(path):
            path = os.path.join(path, cls.FFINFO_FILENAME)

        work_path = os.path.dirname(path)
        if not os.path.exists(work_path):
            if mkdir:
                os.makedirs(work_path)
            elif must_exist:
                raise FFManagerError('Work directory "{}" does not exist!'.format(work_path))

        if not os.path.exists(path) and must_exist:
            raise FFManagerError('FFInfo file "{}" does not exist!'.format(path))

        return path

    @classmethod
    def read_ffinfo(cls, path):
        path = cls.get_ffinfo_path(path, must_exist=True)
        with open(path, 'rb') as f:
            return json.loads(f.read().decode('utf-8'))

    @classmethod
    def write_ffinfo(cls, data, path):
        path = cls.get_ffinfo_path(path, mkdir=True)

        def json_serial(obj):
            if isinstance(obj, (datetime.datetime, datetime.date)):
                return cls.strftime(obj)
            raise TypeError('Type {} not serializable'.format(type(obj)))

        with open(path, 'wb') as f:
            f.write(json.dumps(
                data,
                indent=4,
                sort_keys=True,
                ensure_ascii=False,
                default=json_serial
            ).encode('utf-8'))

    # tools

    @classmethod
    def strftime(cls, dt):
        if not dt:
            return None
        elif isinstance(dt, str):
            return dt
        else:
            return dt.strftime(cls.DATETIME_FORMAT)

    @classmethod
    def strptime(cls, s):
        if not s:
            return None
        elif isinstance(s, datetime.datetime):
            return s
        else:
            return datetime.datetime.strptime(s, cls.DATETIME_FORMAT)

    @classmethod
    def create_work_id(cls, series, name):
        name = name if name else 'UNDEFINED'
        series = series if series else ['UNDEFINED', ]

        work_id = '{} - {}'.format(' + '.join(series), name)
        work_id = cls.sanitize_string(work_id, remove_spaces=True)

        return work_id

    @classmethod
    def create_chapter_id(cls, number, suffix=None):
        chapter_id = '{:05d}{}'.format(number, '_'+suffix if suffix else '')
        chapter_id = cls.sanitize_string(chapter_id, remove_spaces=True)
        return chapter_id

    @classmethod
    def sanitize_string(cls, string, remove_spaces=False, remove_comas=False):
        # strip white characters from start/end
        string = string.strip()

        # replace ntfs illegal characters
        illegal = ['<', '>', ':', '"', '/', '\\', '|', '?', '*']
        for ch in illegal:
            string = string.replace(ch, '_')

        # replace tab with space
        string = string.replace('\t', ' ')

        # remove spaces
        if remove_spaces:
            string = string.replace(' ', '_')

        # remove comas
        if remove_comas:
            string = string.replace('\'', '_').replace('"', '_')

        return string

    # download

    def download(self, download_metadata=False, check_chapters=False, download_chapters=False, download_range=None,
                 dry_run=False, detailed_chapter_list=False):
        """
        Should raise exception if there is any error.
        :param download_metadata: Downloads work metadata
        :param check_chapters: Prints list of missing chapters
        :param download_chapters: Downloads missing chapters
        :param download_range: Chapter download range (selects all if undefined)
        :param dry_run: If True does not write anything to disk + disables chapter download
        :param detailed_chapter_list: Printed list of missing chapters will be more detailed
        """
        download_metadata = download_metadata or check_chapters or download_chapters
        check_chapters = check_chapters or download_chapters
        download_range = download_range or [1, -1]  # NOTE: chapter range starts at 1
        if not download_metadata and not check_chapters and not download_chapters:
            return

        if self.id:
            _logger.info('Processing work "{}"'.format(self.id))
        else:
            _logger.info('Processing new work')

        # init parser

        parser = init_parser(self.url, self.options)
        _logger.info('Using parser "{}"'.format(parser.NAME))

        # download, load and save metadata

        if download_metadata:

            def update_metadata_dict(old, new, depth=0):
                if depth >= 50:
                    raise FFManagerError('Detected possible recursion loop')

                for key in new:
                    # add new keys
                    if key not in old:
                        old[key] = new[key]
                        continue

                    # do not delete manually created values
                    if new[key] is None and old[key] is not None:
                        continue

                    # do not updated id values
                    if depth == 0 and key in ['id', 'name', 'series']:
                        if old[key] and old[key] != new[key]:
                            _logger.info('ID value "{}" is different: "{}" != "{}"'.format(key, old[key], new[key]))
                            continue
                        elif not old[key] and old[key] != new[key]:
                            _logger.info('New ID value "{}": "{}"'.format(key, new[key]))

                    # recursion
                    if isinstance(new[key], dict):
                        if isinstance(old[key], dict):
                            update_metadata_dict(old[key], new[key], depth=depth+1)
                            continue

                    # update value
                    if depth == 0 and key in ['chapters', 'update_datetime'] and old[key] != new[key]:
                        _logger.info('Value "{}" changed: {} -> {}'.format(
                            key,
                            self.strftime(old[key]) if isinstance(old[key], datetime.datetime) else old[key],
                            self.strftime(new[key]) if isinstance(new[key], datetime.datetime) else new[key]
                        ))
                    old[key] = new[key]

            # download new metadata

            new_metadata = parser.get_metadata()

            # calculate/update some new metadata values

            new_metadata['options'] = parser.options
            if 'series' in new_metadata:
                new_metadata['series'].sort()
            new_metadata['id'] = self.create_work_id(
                new_metadata.get('series'),
                new_metadata.get('name')
            )
            new_metadata['download_datetime'] = datetime.datetime.utcnow()

            # update old metadata with new metadata

            metadata = copy.deepcopy(self.metadata)
            update_metadata_dict(metadata, new_metadata)

            # load and save updated metadata

            self.load_metadata(metadata=metadata)
            self.save_metadata(write_to_file=not dry_run)

        # check/download chapters

        if check_chapters or download_chapters:

            chapter_list = parser.get_chapter_list()

            # get list of already downloaded chapters
            downloaded_chapters = []

            if self.path and os.path.exists(self.path):
                for filename in os.listdir(self.path):
                    if not os.path.isfile(os.path.join(self.path, filename)):
                        continue
                    if '.' in filename and filename.split('.')[-1] in self.CHAPTER_EXTENSION_BLACKLIST:
                        continue
                    if filename == self.FFINFO_FILENAME:
                        continue

                    number_str = filename
                    for sep in ['.', '_', ' ', '-']:
                        number_str = number_str.split(sep)[0]
                    number = int(number_str) if number_str.isdigit() else None

                    if number is not None:
                        downloaded_chapters.append(number)

            downloaded_chapters = list(set(downloaded_chapters))

            # fix download range (convert negative idx to positive idx)
            if download_range[0] < 0:
                download_range[0] = len(chapter_list) + (download_range[0]+1)
            if download_range[1] < 0:
                download_range[1] = len(chapter_list) + (download_range[1]+1)

            # get list of chapters to download
            chapters_to_download = []
            for chapter in chapter_list:
                if chapter['number'] in downloaded_chapters:
                    continue
                if chapter['number'] < download_range[0]:
                    continue
                if chapter['number'] > download_range[1]:
                    continue
                chapters_to_download.append(chapter)

            # print list of chapters to download
            if len(chapters_to_download) == 0:
                _logger.info('No missing chapters to download')
                return
            elif detailed_chapter_list:
                _logger.info('Found {} chapters:'.format(len(chapters_to_download)))
                for ch in chapters_to_download:
                    _logger.info('{} - {}'.format(ch['number'], ch['name']))
            else:
                chapter_ids = [str(ch['number']) for ch in chapters_to_download]
                _logger.info('Chapters to download: {}'.format(','.join(chapter_ids)))

            # download chapters
            if download_chapters and not dry_run:
                _logger.info('Downloading list of {} chapters,'.format(len(chapters_to_download)))
                _logger.info('from: {}; to: {}'.format(chapters_to_download[0]['name'], chapters_to_download[-1]['name']))

                for ch in chapters_to_download:
                    _logger.info('Downloading chapter {} - {}'.format(ch['number'], ch['name']))

                    filename = '{}.html'.format(self.create_chapter_id(ch['number'], ch['suffix']))
                    filepath = os.path.join(self.path, filename)

                    ch_html = parser.get_chapter(ch)['html']
                    with open(filepath, 'wb') as f:
                        f.write(ch_html)
