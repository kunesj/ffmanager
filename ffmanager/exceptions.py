#!/usr/bin/env python3
# coding: utf-8

import logging

_logger = logging.getLogger(__name__)


class FFManagerError(Exception):
    pass  # base exception class for this module


class ParserError(FFManagerError):
    pass
