#!/usr/bin/env python3
# encoding: utf-8

import logging
import sys
import os
import argparse

from .misctools.logging_color import setup_logging
from .database import Database


def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter,
        description='FFManager\n\n'
                    'Small script for keeping fiction and fanfiction downloaded from internet updated.'
    )
    parser.add_argument(
        'path',
        default='.',
        type=str, nargs='?',
        help='Path to folder with works or path to work'
    )
    parser.add_argument(
        '-u', '--url',
        default=None,
        help='Url of new fanfiction series to download (requires --download for actual download of chapters)')
    parser.add_argument(
        '-c', '--check',
        action='store_true',
        help='Will check for updates (downloads metadata)')
    parser.add_argument(
        '-d', '--download',
        action='store_true',
        help='Will download new chapters (forces --check)')
    parser.add_argument(
        '-r', '--range',
        default=[1, -1],
        type=int, metavar='N', nargs='+',
        help='Range of chapters to download [1, -1], range starts at 1.')
    parser.add_argument(
        '-f', '--force',
        action='store_true',
        help='Will disable skipping of recently updated works')
    parser.add_argument(
        '--debug',
        action='store_true',
        help='Debug mode')
    parser.add_argument(
        '--logfile',
        action='store_true',
        help='Save log to file')
    args = parser.parse_args()

    setup_logging()
    _logger = logging.getLogger()

    _logger.setLevel(logging.INFO)
    if args.debug:
        _logger.setLevel(logging.DEBUG)

    _requests_logger = logging.getLogger('requests')
    if _requests_logger.getEffectiveLevel() < logging.WARNING:
        _requests_logger.setLevel(logging.WARNING)

    if args.logfile:
        try:
            os.remove('ffmanager.log')
        except OSError:
            pass
        fh = logging.FileHandler('ffmanager.log')
        fh.setFormatter(logging.Formatter(fmt='%(levelname)s:%(name)s:%(message)s'))
        _logger.addHandler(fh)

    if not os.path.exists(args.path):
        _logger.error('Path "{}" does not exist'.format(args.path))
        sys.exit(2)

    if args.range and len(args.range) != 2:
        _logger.error('Bad download range!')
        sys.exit(2)

    # init database

    database = Database(args.path)

    # download new work

    if args.url:
        database.download_new_work(
            args.url,
            check_chapters=True,
            download_chapters=args.download,
            download_range=args.range,
            dry_run=not args.download,
            detailed_chapter_list=not args.download,
        )
        return

    # update database works

    if args.check or args.download:
        database.download(
            force=args.force,
            download_metadata=args.check,
            check_chapters=args.check,
            download_chapters=args.download,
            download_range=args.range
        )
        return

    _logger.info('Nothing to do')


if __name__ == "__main__":
    main()
