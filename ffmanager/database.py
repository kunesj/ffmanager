#!/usr/bin/env python3
# coding: utf-8

import logging
import os
import datetime

from .exceptions import FFManagerError, ParserError
from .work import Work

_logger = logging.getLogger(__name__)


class Database(object):

    DOWNLOAD_EVERY_N_DAYS_MIN = 1
    DOWNLOAD_EVERY_N_DAYS_MAX = 30

    def __init__(self, root_path):
        if not os.path.exists(root_path) or not os.path.isdir(root_path):
            raise FFManagerError('Invalid database root path "{}"!'.format(root_path))
        self.root_path = os.path.abspath(root_path)

        self.works = {}
        self.load_works()

    def load_works(self):
        self.works = {}
        for root, subFolders, files in os.walk(self.root_path):
            if Work.is_work(root):
                work = Work(self.root_path, path=root)
                if not work.id:
                    _logger.error('Work at path "{}" has undefined ID!'.format(root))
                    continue
                if work.id in self.works:
                    _logger.warning('Detected duplicate works in "{}" and "{}"!'.format(work.path, self.works[work.id].path))
                self.works[work.id] = work

    def download(self, force=False, **kwargs):
        _logger.info('Recursively updating works in path "{}"'.format(self.root_path))
        _logger.info('Download mode: {}'.format(' '.join(['{}={}'.format(key, kwargs[key]) for key in kwargs])))

        if not force and kwargs.get('download_chapters'):
            _logger.info('Chapter download mode forces force=True')
            force = True

        if len(self.works) == 0:
            _logger.info('-------------------------------------------------------------------')
            _logger.info('Database is empty - Nothing to update')

        for work_id in self.works:

            if not self.works[work_id].url or self.works[work_id].options.get('disable'):
                _logger.info('-------------------------------------------------------------------')
                _logger.info('Download disabled for work "{}"'.format(work_id))
                continue

            if self.works[work_id].update_datetime and not force:
                # fix bad time values
                if self.works[work_id].update_datetime and not self.works[work_id].download_datetime:
                    self.works[work_id].download_datetime = self.works[work_id].update_datetime
                if self.works[work_id].update_datetime > datetime.datetime.utcnow():
                    self.works[work_id].update_datetime = datetime.datetime.utcnow()
                if self.works[work_id].download_datetime > datetime.datetime.utcnow():
                    self.works[work_id].download_datetime = datetime.datetime.utcnow()
                if self.works[work_id].update_datetime > self.works[work_id].download_datetime:
                    self.works[work_id].download_datetime = self.works[work_id].update_datetime

                # calculate time deltas
                td_update_to_download = self.works[work_id].download_datetime - self.works[work_id].update_datetime
                td_download_to_now = datetime.datetime.utcnow() - self.works[work_id].download_datetime

                # should we skip download for this work?
                if td_download_to_now.days < self.DOWNLOAD_EVERY_N_DAYS_MIN or \
                   td_download_to_now.days < min(td_update_to_download.days, self.DOWNLOAD_EVERY_N_DAYS_MAX):
                    continue

            _logger.info('-------------------------------------------------------------------')
            try:
                self.works[work_id].download(**kwargs)
            except (FFManagerError, ParserError) as e:
                _logger.error(str(e))
                continue
            except Exception:
                _logger.exception('Work download failed!')
                continue

        _logger.info('-------------------------------------------------------------------')

    def download_new_work(self, url, **kwargs):
        work = Work(self.root_path)
        work.url = url

        try:
            work.download(**kwargs)
            if kwargs.get('dry_run'):
                _logger.info('Run again with "--download" parameter to download chapters')
        except (FFManagerError, ParserError) as e:
            _logger.error(str(e))
            return
        except Exception:
            _logger.exception('Work download failed!')
            return

        if work.id and not kwargs.get('dry_run'):
            if work.id in self.works:
                if work.path != self.works[work.id].path:
                    _logger.warning('Detected duplicate works in "{}" and "{}"!'.format(work.path, self.works[work.id].path))
                else:
                    _logger.warning('Updated already existing work at "{}"!').format(work.path)
            self.works[work.id] = work
