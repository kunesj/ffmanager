#!/usr/bin/env python3
# encoding: utf-8

import unittest

from ffmanager.parsers import get_parser_class

# always check if url is OK if there is test error
data = {
    'fanfiction.net': [
        get_parser_class('fanfiction.net'),
        'https://www.fanfiction.net/s/7347955/1/Dreaming-of-Sunshine'
    ],
    'fictionpress.com': [
        get_parser_class('fictionpress.com'),
        'https://www.fictionpress.com/s/2961893/1/Mother-of-Learning'
    ],
    'archiveofourown.org': [
        get_parser_class('archiveofourown.org'),
        'https://archiveofourown.org/works/2080878'
    ],
    'adult-fanfiction.org': [
        get_parser_class('adult-fanfiction.org'),
        'http://anime.adult-fanfiction.org/story.php?no=600047013'
    ],
    'royalroad.com': [
        get_parser_class('royalroad.com'),
        'https://www.royalroad.com/fiction/21220/mother-of-learning'
    ],
    'fanfics.me': [
        get_parser_class('fanfics.me'),
        'http://fanfics.me/read2.php?id=190897'
    ],
    'questionablequesting.com': [
        get_parser_class('questionablequesting.com'),
        'https://forum.questionablequesting.com/threads/dwarf-of-bronze-znt-au.2659/'
    ],
    'sufficientvelocity.com': [
        get_parser_class('sufficientvelocity.com'),
        'https://forums.sufficientvelocity.com/threads/dwarf-of-bronze-znt.25023/'
    ],
    'spacebattles.com': [
        get_parser_class('spacebattles.com'),
        'https://forums.spacebattles.com/threads/this-sceptered-moon-timeline-story-stargate-au-britwank.394374/'
    ],
}


class ParsersTest(unittest.TestCase):
    """
    Run only this test class:
        nosetests3 -v -s tests.parsers_test
        nosetests3 -v -s --logging-level=DEBUG tests.parsers_test
    Run only single test:
        nosetests3 -v -s tests.parsers_test:ParsersTest.test_FanFictionNetParser
        nosetests3 -v -s --logging-level=DEBUG tests.parsers_test:ParsersTest.test_FanFictionNetParser
    """

    def test_FanfictionNetParser(self):
        self.genericParserTest(*data['fanfiction.net'])

    def test_FictionpressComParser(self):
        self.genericParserTest(*data['fictionpress.com'])

    def test_ArchiveofourownOrgParser(self):
        self.genericParserTest(*data['archiveofourown.org'])

    def test_AdultfanfictionOrgParser(self):
        self.genericParserTest(*data['adult-fanfiction.org'])

    def test_RoyalroadlParser(self):
        self.genericParserTest(*data['royalroad.com'])

    def test_FanficsMeParser(self):
        self.genericParserTest(*data['fanfics.me'])

    def test_QuestionablequestingComParser(self):
        self.genericParserTest(*data['questionablequesting.com'])

    def test_SufficientvelocityComParser(self):
        self.genericParserTest(*data['sufficientvelocity.com'])

    def test_SpacebattlesComParser(self):
        self.genericParserTest(*data['spacebattles.com'])

    def genericParserTest(self, parser_class, url):
        self.assertIsNotNone(parser_class)

        self.assertTrue(parser_class.has_url(url))
        parser = parser_class(url)

        metadata = parser.get_metadata()
        self.assertTrue(isinstance(metadata, dict))

        ch_list = parser.get_chapter_list()
        self.assertEqual(len(ch_list), metadata['chapters'])

        chapter = parser.get_chapter(ch_list[0])
        self.assertTrue(bool(chapter['html']))
